<?php

/**
 * @file
 * Functions to add/edit variables before passing to twig.
 */

/**
 * Implements hook_preprocess_HOOK() for paragraph templates.
 */
function ucisom_preprocess_media(&$variables) {
  $media = $variables['media'];
  $type = $media->bundle();
  $view_mode = $variables['view_mode'];
  if ($type == 'remote_video') {
    $video = $media->field_media_oembed_video->getValue()[0]['value'];
    $splash_image = get_image_object($media->field_splash_image->entity->field_media_image->first()->getvalue(), $view_mode);
    $variables['video'] = $video;
    $variables['splash_image'] = $splash_image;
    $variables['additional_classes'] = $view_mode;
  }
}
