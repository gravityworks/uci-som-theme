<?php

/**
 * @file
 * Functions to add/edit variables before passing to twig.
 */

use Drupal\file\Entity\File;
use Drupal\image\Entity\ImageStyle;
use Drupal\config_pages\Entity\ConfigPages;

/**
 * Implements hook_preprocess_HOOK() for paragraph templates.
 */
function ucisom_preprocess_paragraph(&$variables) {
  $paragraph = $variables['paragraph'];
  $type = $paragraph->bundle();

  if ($paragraph->hasField('field_image') && !$paragraph->field_image->isEmpty()) {
    if ($paragraph->field_image->entity->field_media_image) {
      $image = $paragraph->field_image->entity->field_media_image->first()->getValue();
      // dd($image, $type);.
      $variables['image'] = get_image_object($image, $type);
    }
  }

  if ($paragraph->hasField('field_callout_5050_item')) {
    $image_1_field = $paragraph->field_callout_5050_item[0]->entity->field_image->entity ? $paragraph->field_callout_5050_item[0]->entity->field_image->entity->field_media_image : NULL;
    $image_2_field = $paragraph->field_callout_5050_item[1]->entity->field_image->entity ? $paragraph->field_callout_5050_item[1]->entity->field_image->entity->field_media_image : NULL;
    $image_1 = $image_1_field ? get_image_object($image_1_field->first()->getValue(), $paragraph->bundle()) : NULL;
    $image_2 = $image_2_field ? get_image_object($image_2_field->first()->getValue(), $type) : NULL;
    $variables['type'] = $type;
    $variables['images_5050'] = [$image_1, $image_2];
  }

  if ($paragraph->hasField('field_background_image') && !$paragraph->field_background_image->isEmpty()) {
    // Paragraph machine name => Image style name.
    $image_style = [
      'callout_gradient' => 'callout_gradient_background',
    ];
    $image = $paragraph->field_background_image->entity->field_media_image->first()->getValue();
    $image_uri = File::load($image['target_id'])->getFileUri();
    $style = ImageStyle::load($image_style[$type]);
    $image_url = !empty($style) ? ImageStyle::load($image_style[$type])->buildUrl($image_uri) : file_url_transform_relative(file_create_url($image_uri));
    $variables['image_background'] = [
      'image_style' => $image_style[$type],
      'image_style-path' => $style,
      'uri' => $image_uri,
      'url' => $image_url,
      'alt' => $image['alt'],
      'width' => $image['width'],
      'height' => $image['height'],
      'type' => $type,
    ];
  }

  if ($paragraph->hasField('field_callout_items') && !$paragraph->field_callout_items->isEmpty() && !$paragraph->field_callout_items->first()->entity->field_image->isEmpty()) {
    $image_style = 'callout_basic';
    $items = $paragraph->field_callout_items;
    $images = [];
    for ($i = 0; $i < count($items); $i++) {
      $image = $items[$i]->entity->field_image->entity->field_media_image->first()->getValue();
      $label = $items[$i]->entity->field_title->getValue();
      // $link = $items[$i]->entity->field_link->uri();
      $image_uri = File::load($image['target_id'])->getFileUri();
      $image_type = substr($image_uri, -3);
      $image_url_raw = file_url_transform_relative(file_create_url($image_uri));
      $image_url = $image_type === 'svg' ? $image_url_raw : ImageStyle::load($image_style)->buildUrl($image_uri);
      $rich_text_field = $items[$i]->entity->field_rich_text->getValue();
      $rich_text = $rich_text_field ? $rich_text_field[0]['value'] : NULL;
      $image_object = [
        'type' => $image_type,
        'label' => $label ? $label[0]['value'] : NULL,
        'style' => $image_style,
        // 'link' => $link,
        'uri' => $image_uri,
        'url' => $image_url,
        'url_raw' => $image_url_raw,
        'alt' => $image['alt'],
        'width' => $image['width'],
        'height' => $image['height'],
        'rich_text' => $rich_text,
      ];
      array_push($images, $image_object);
    }
    $variables['callout_images'] = $images;
  };

  if ($paragraph->hasField('field_slides') && !$paragraph->field_slides->isEmpty()) {
    $image_style = 'carousel_image';
    $items = $paragraph->field_slides;
    $slides = [];
    for ($i = 0; $i < count($items); $i++) {
      $image = $items[$i]->entity->field_image->entity->field_media_image->first()->getValue();
      $caption = !$items[$i]->entity->field_body->isEmpty() ? $items[$i]->entity->field_body->first()->getValue() : ['value' => ''];
      // $link = $items[$i]->entity->field_link->uri();
      $image_uri = File::load($image['target_id'])->getFileUri();
      $image_type = substr($image_uri, -3);
      $image_url_raw = file_url_transform_relative(file_create_url($image_uri));
      $image_url = $image_type === 'svg' ? $image_url_raw : ImageStyle::load($image_style)->buildUrl($image_uri);
      $slide_object = [
        'type' => $image_type,
        // 'label' => $label[0]['value'],
        // 'link' => $link,
        'uri' => $image_uri,
        'img_src' => $image_url,
        'url_raw' => $image_url_raw,
        'img_alt' => $image['alt'],
        'width' => $image['width'],
        'height' => $image['height'],
        'caption' => $caption['value'],
      ];
      array_push($slides, $slide_object);
    }
    $variables['slides'] = $slides;
  };

  if ($paragraph->hasField('field_splash') && !$paragraph->field_splash->isEmpty()) {
    $image_style = 'full_width';
    $item = $paragraph->field_splash;
    $image = $item->entity->field_image->entity->field_media_image->first()->getValue();
    $caption = $item->entity->field_body->first() ? $item->entity->field_body->first()->getValue() : '';
    $image_uri = File::load($image['target_id'])->getFileUri();
    $image_url = ImageStyle::load($image_style)->buildUrl($image_uri);
    $variables['splash_image'] = [
      'img_src' => $image_url,
      'img_alt' => $image['alt'],
      'width' => $image['width'],
      'height' => $image['height'],
      'caption' => $caption['value'],
    ];
  };

  if ($paragraph->hasField('field_faculty_staff_members') && !$paragraph->field_faculty_staff_members->isEmpty()) {
    $faculty = $paragraph->field_faculty_staff_members;
    $items = [];
    foreach ($faculty as $member) {
      $person = $member->entity->field_faculty_staff_member->entity;
      if (is_null($person)) {
        continue;
      }

      /* Add conditional to check whether a Person is published/unpublished */
      if (!$person->isPublished()) {
        continue;
      }

      $type = $person->bundle() === 'faculty' ? 'faculty' : 'staff';
      $nid = $person->id();
      $url = \Drupal::service('path_alias.manager')->getAliasByPath('/node/' . $nid);

      $first_name = $person->field_first_name->getString();
      $last_name = $person->field_last_name->getSTring();
      $name = $first_name . " " . $last_name;
      $credentials = $person->field_degree->getString();
      $unique_id = $person->field_unique_id ? $person->field_unique_id->getString() : NULL;

      if ($credentials) {
        $name .= ', ' . $credentials;
      }
      $image_url = '';
      if ($person->field_profile_image->entity) {
        $image_uri = File::load($person->field_profile_image->entity->field_media_image[0]->target_id)->getFileUri();
        $style = ImageStyle::load('profile_image');
        $image_url = $style->buildUrl($image_uri);
      }

      $member_object = [
        'name' => $name,
        'working_titles' => $person->field_working_title->getValue(),
        'img_src' => $image_url,
        'url' => $url,
        'unique_id' => $unique_id,
        'type' => $type,
        'published' => $person->isPublished(),
      ];
      array_push($items, $member_object);
    }
    $variables['faculty'] = $items;
  }
}

/**
 * Implements hook_preprocess_HOOK() for global variables.
 */
function ucisom_preprocess(&$variables) {
  $site_config = \Drupal::config('system.site');
  $variables['site_name'] = $site_config->get('name');
  $variables['site_slogan'] = $site_config->get('slogan');
  $variables['site_url'] = \Drupal::request()->getScheme() . '://' . \Drupal::request()->getHost();
  $variables['site_logo'] = file_url_transform_relative(file_create_url(theme_get_setting('logo.url')));
  $variables['site_modifier'] = theme_get_setting('site_modifier') == 1 ? 'institute-center' : '';
  $socialMedia = ConfigPages::config('social_media');
  $social_media_links = [];
  if (!empty($socialMedia)) {
    foreach ($socialMedia->get('field_site_wide_social_media') as $link) {
      $link_object = [
        'social_icon' => $link->entity->field_template_icon->value,
        'link_url' => $link->entity->field_link->first()->getValue()['uri'],
      ];
      array_push($social_media_links, $link_object);
    }
  }
  $variables['social_media'] = $social_media_links;
  $placeholderImage = ConfigPages::config('placeholder_image');
  $personPlaceholderImage = ConfigPages::config('placeholder_person_image');
  $variables['placeholder_image'] = $placeholderImage;
  $variables['placeholder_person_image'] = $personPlaceholderImage;
}

/**
 * Implements hook_preprocess_HOOK() for meta tags.
 */
function ucisom_preprocess_html(&$variables) {
  $site_config = \Drupal::config('system.site');
  $site_name = strtolower($site_config->get('name'));
  if (!empty($site_name)) {
    $name = str_replace(' ', '-', $site_name);
    $variables['attributes']['class'][] = 'site--' . $name;
  }
  $disablePhoneDetection = [
    '#tag' => 'meta',
    '#attributes' => [
      'name' => 'format-detection',
      'content' => 'telephone=no',
    ],
  ];

  $variables['page']['#attached']['html_head'][] = [
    $disablePhoneDetection, 'format-detection',
  ];

  foreach ($variables['user']->getRoles() as $role) {
    $variables['attributes']['class'][] = 'role-' . $role;
  }

  // Add unique classes for each page.
  $path = \Drupal::service('path.current')->getPath();
  $alias = \Drupal::service('path_alias.manager')->getAliasByPath($path);
  $alias = trim($alias, '/');
  if (!empty($alias)) {
    $name = str_replace('/', '-', $alias);
    $variables['attributes']['class'][] = 'page-' . $name;
  }
};

/**
 * Implements hook_preprocess_region().
 */
function ucisom_preprocess_region(array &$variables, $hook) {
  if ($node = \Drupal::request()->attributes->get('node')) {
    $variables['nid'] = $node->id();
    $variables['type'] = $node->bundle();
  }
  try {
    $variables['is_front'] = \Drupal::service('path.matcher')->isFrontPage();
  }
  catch (Exception $e) {
    // If the database is not yet available, set default values for these
    // variables.
    $variables['is_front'] = FALSE;
  }
}

/**
 * Helper function to return an image object from an image field.
 */
function get_image_object($image, $type) {
  // Paragraph machine name => Image style name.
  $image_style = [
    'blockquote' => 'blockquote_image',
    'mini_menu_with_callout' => 'callout_mini_menu',
    'callout_50_50' => 'callout_50_50',
    'callout_50_50_item' => 'callout_50_50',
    'callout_gradient' => 'callout_gradient',
    'callout_featured' => 'callout_featured',
    'image_with_caption' => 'full_width',
    'full_width_image' => 'full_width',
    'hero' => 'full_width',
    'carousel' => 'carousel_image',
    'callout_photo' => 'callout_photo',
    'news_thumbnail' => 'search_result_thumbnail',
    'news_full' => 'full_width',
    'video_splash_image' => 'video_splash_image',
    'wysiwyg_full_width' => 'wysiwyg_full_width',
    'wysiwyg_three_quarter_width' => 'wysiwyg_three_quarter_width',
    'wysiwyg_half_width' => 'wysiwyg_half_width',
    'wysiwyg_quarter_width' => 'wysiwyg_quarter_width',
    'persona_item' => 'persona_background',
    'persona_item_content' => 'callout_photo',
    'events_teasers' => 'large',
    'event_speaker' => 'large',
    'event_sponsor' => 'large',
    'content_with_image' => 'full_width',
  ];
  $image_uri = File::load($image['target_id'])->getFileUri();
  $style = ImageStyle::load($image_style[$type]);
  $image_url = !empty($style) ? ImageStyle::load($image_style[$type])->buildUrl($image_uri) : file_url_transform_relative(file_create_url($image_uri));
  return [
    'image_style' => $image_style[$type],
    'image_style-path' => $style,
    'uri' => $image_uri,
    'url' => $image_url,
    'alt' => $image['alt'],
    'width' => $image['width'],
    'height' => $image['height'],
    'type' => $type,
  ];
};
