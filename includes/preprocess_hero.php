<?php

/**
 * @file
 * Functions to create landing page menus.
 */

use Drupal\Core\Menu\MenuTreeParameters;
use Drupal\node\Entity\Node;

/**
 * Implements hook_preprocess_HOOK() for global variables.
 */
function ucisom_preprocess_paragraph__hero(&$variables) {
  // Microsite Menu System.
  $node = \Drupal::request()->attributes->get('node');
  if ($node) {
    $variables['node'] = $node;
    $node_id = $node->id();
    $isFront = \Drupal::service('path.matcher')->isFrontPage($node);
    $variables['node_is_front'] = $isFront;
    $type = $node->getType();
    if ($type == 'event') {
      $variables['title_override'] = $node->getTitle();
    }
    elseif ($type == 'page') {
      $landing_page_menu = get_landing_page_menu($node_id);
      if ($landing_page_menu) {
        $variables['landing_page_menu'] = $landing_page_menu->getUrlObject()->toString();
        $variables['landing_page_title'] = $landing_page_menu->getTitle();
        $menu_tree_service = \Drupal::service('menu.link_tree');
        $menu_root = get_landing_page_sub_menu_root($landing_page_menu);
        $variables['menu_root'] = $menu_root;
        if ($menu_root && $menu_root->hasChildren) {
          $sub_menu = get_landing_page_sub_menu($menu_root);
          $variables['sub_menu'] = $menu_tree_service->build($sub_menu);
        }
      }
    }
  }
}

/**
 * Helper function to get parent node based on menu hierarchy.
 */
function get_parent_node($child_node_id) {
  /** @var \Drupal\Core\Menu\MenuLinkManagerInterface $menu_link_manager */
  $menu_link_manager = \Drupal::service('plugin.manager.menu.link');
  $link = get_node_menu_info($child_node_id);

  // Now check if the menu has a parent menu item and if so load it.
  /** @var \Drupal\Core\Menu\MenuLinkInterface $parent */
  if ($link && $link->getParent() && $parent = $menu_link_manager->createInstance($link->getParent())) {
    // Finally, we figure out if the parent menu item refers to another node
    // and if so, load it.
    $route = $parent->getUrlObject()->getRouteParameters();

    if (isset($route['node']) && $parent_node = Node::load($route['node'])) {
      return($parent_node);
      // We now have a fully loaded node in the $parent_node variable and can
      // get whatever data we need from it.
    }
  }
}

/**
 * Helper function to get node menu info based on node id.
 */
function get_node_menu_info($node_id) {
  /** @var \Drupal\Core\Menu\MenuLinkManagerInterface $menu_link_manager */
  $menu_link_manager = \Drupal::service('plugin.manager.menu.link');
  $links = $menu_link_manager->loadLinksByRoute('entity.node.canonical', ['node' => $node_id]);
  // Because loadLinksByRoute() returns an array keyed by a complex id
  // it is simplest to just get the first result by using array_pop().
  /** @var \Drupal\Core\Menu\MenuLinkInterface $link */
  $link = array_pop($links);
  return($link);
}

/**
 * Helper function to get the menu from the parent element landing page.
 */
function get_landing_page_menu($node_id) {
  $current_node = Node::load($node_id);
  // Check if the current page is a landing page and, if so, get it's menu
  // But don't show the menu if the landing page has no parents.
  if (is_landing_page($current_node)) {
    return get_node_menu_info($node_id);
  }
  // Otherwise, keep checking through the parents until you find a landing page.
  else {
    $parent_node = get_parent_node($node_id);
    while ($parent_node) {
      if (is_landing_page($parent_node)) {
        return get_node_menu_info($parent_node->id());
      }
      else {
        $parent_node = get_parent_node($parent_node->id());
      }
    };
  }
}

/**
 * Helper function to determine if a page is a landing page.
 */
function is_landing_page(&$node_id) {
  if ($node_id->field_is_landing_page->value == "1") {
    return TRUE;
  }
  else {
    return FALSE;
  }
}

/**
 * Helper function to get the root element for the sub-menu.
 */
function get_landing_page_sub_menu_root(&$landing_page_menu) {
  $menu_parameters = new MenuTreeParameters();
  $menu_parameters->setMaxDepth(10);
  $menu_tree_service = \Drupal::service('menu.link_tree');
  $tree = $menu_tree_service->load('main', $menu_parameters);
  return parse_menus($tree, $landing_page_menu);
}

/**
 * Helper function to recursively go through each of the menu items.
 */
function parse_menus(&$tree, $landing_page_menu) {
  foreach ($tree as $value) {
    if ($value->link->getUrlObject() == $landing_page_menu->getUrlObject()) {
      return $value;
    }
    elseif ($value->hasChildren) {
      $var1 = $value->subtree;
      $var2 = $landing_page_menu;
      $child = parse_menus($var1, $var2);
      if ($child !== NULL) {
        return $child;
      }
    }
  }
}

/**
 * Helper function to get the sub-menu.
 */
function get_landing_page_sub_menu($landing_page_sub_menu_root) {
  $menu_id = $landing_page_sub_menu_root->link->getPluginId();
  $sub_menu_parameters = new MenuTreeParameters();
  $sub_menu_parameters->setRoot($menu_id);
  $sub_menu_parameters->setMaxDepth(2);
  $menu_tree_service = \Drupal::service('menu.link_tree');
  $manipulators = [
    ['callable' => 'menu.default_tree_manipulators:generateIndexAndSort'],
  ];
  $tree = $menu_tree_service->load('main', $sub_menu_parameters);
  $tree = $menu_tree_service->transform($tree, $manipulators);
  $variables['tree'] = $tree;
  return $tree;
}
