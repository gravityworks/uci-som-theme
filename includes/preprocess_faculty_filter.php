<?php

/**
 * @file
 * Functions to add/edit variables before passing to twig.
 *
 * Opted to not use this to leave control in the view interface.
 */

/**
 * Implements hook_preprocess_HOOK() for exposed views filters.
 */
function ucisom_preprocess_views_exposed_form(&$variables) {
  $term_ids_to_keep = ['77', '2'];
  $form =& $variables['form'];
  if (array_key_exists('name', $form) && $form['name']['#context']['#view_id'] === 'faculty') {
    $departments = $form['department']['#options'];
    print_r($departments);
    // $all = $departments['All'];
    // unset($departments['All']);
    $filtered_departments = array_filter($departments, "filter_term_by_parents");
    // $filtered_departments = array_unshift($filtered_departments, 'bubu');
    // $filtered_departments = $all;
    dd($filtered_departments);
    $form['department']['#options'] = $filtered_departments;
  }
};

/**
 * Filter departments to certain taxonomy parents.
 */
function filter_term_by_parents($term) {
  // Basic Science Departments - Term 77.
  // Clinical Departments - Term 2.
  $term_ids_to_keep = ['77', '2'];
  foreach ($term->option as $term_id => $term_value) {
    $ancestors = \Drupal::service('entity_type.manager')->getStorage("taxonomy_term")->loadAllParents($term_id);
    if (count($ancestors) > 1) {
      $top_level_parent = end($ancestors);
      $top_level_parent_tid = $top_level_parent->tid->getValue()[0]['value'];
      if (in_array($top_level_parent_tid, $term_ids_to_keep)) {
        return $term;
      }
    };
  };
};
