<?php

/**
 * @file
 * Functions to create landing page menus.
 */

/**
 * Implements hook_reprocess_HOOK().
 */

/**
 * Talk to Justin if revisiting getting breadcrumbs and Ajax to play nicely.
 */
function ucisom_preprocess_views_view(&$variables) {
  $variables['bubu'] = \Drupal::service('breadcrumb')
    ->build(\Drupal::routeMatch())
    ->toRenderable();
};
