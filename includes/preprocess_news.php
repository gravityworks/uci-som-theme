<?php

/**
 * @file
 * Functions to create landing page menus.
 */

use Drupal\config_pages\Entity\ConfigPages;
use Drupal\views\Views;
use Drupal\file\Entity\File;

/**
 * Implements hook_preprocess_HOOK() for global variables.
 */
function ucisom_preprocess_node__news(&$variables) {
  $node = $variables['node'];
  if ($node->field_news_image->entity) {
    $image = $node->field_news_image->entity->field_media_image->first()->getValue();
    $caption = $node->field_news_image->entity->field_image_caption_long->value;
    $credit = $node->field_news_image->entity->field_image_credit->value;
    $image_full = get_image_object($image, 'news_full');
    $image_thumb = get_image_object($image, 'news_thumbnail');
    $variables['image_full'] = [
      'src' => $image_full['url'],
      'alt' => $image_full['alt'],
      'caption' => $caption,
      'credit' => $credit,
    ];
    $variables['image_thumb'] = [
      'src' => $image_thumb['url'],
      'alt' => $image_thumb['alt'],
      'caption' => $caption,
      'credit' => $credit,
    ];
  }
  $assoc_links = $node->field_links;
  $links = [];
  foreach ($assoc_links as $link) {
    $obj = [
      'url' => $link->uri,
      'title' => $link->title,
    ];
    array_push($links, $obj);
  }
  $variables['related_links'] = $links;
  $assoc_documents = $node->field_document;
  $documents = [];
  foreach ($assoc_documents as $document) {
    $document_title = $document->entity->name->getValue();
    $document_description = $document->entity->field_media_document->description;
    $value = $document->entity->field_media_document->first()->getValue();
    $document_uri = File::load($value['target_id'])->getFileUri();
    $document_url = file_create_url($document_uri);
    $obj = [
      'url' => $document_url,
      'title' => $document_description ?: $document_title[0]['value'],
      // 'title' => $document->title,
    ];
    array_push($documents, $obj);
  }
  $variables['related_documents'] = $documents;
  load_config('about_ucisom', $variables);
  load_config('media_contacts', $variables);
  $related_terms = $node->field_topics;
  $related_term_ids = [];
  foreach ($related_terms as $term) {
    array_push($related_term_ids, $term->target_id);
  }
  $view = load_view('news', 'relatedstories', $related_term_ids);
  $result = $view->result;
  $title = $view->getTitle();
  $results = [];
  foreach ($result as $value) {
    $entity = $value->_entity;
    array_push($results, [
      'title' => $entity->getTitle(),
      'url' => $entity->path->getString(),
    ]);
  }
  $variables['related_news'] = $results;
  $variables['related_news_title'] = $title;
}

/**
 * Helper function to return a config object title and body.
 */
function load_config($config_name, &$variables) {
  $config = ConfigPages::config($config_name);
  if ($config) {
    $title_field = $config->get('field_title');
    $body_field = $config->get('field_body');

    $variables[$config_name] = [
      'title' => $title_field ? $title_field->value : '',
      'body' => ($body_field && $body_field->first()) ? $body_field->first()->value : '',
    ];
  }
}

/**
 * Helper function to load a view of a particular display.
 */
function load_view($view_name, $render_name, $args) {
  $view = Views::getView($view_name);
  $view->setDisplay($render_name);
  $view->setArguments($args);
  $view->execute();
  return $view;
}

/**
 * Preprocess hook for all views.
 */
function ucisom_preprocess_views_view(&$variables) {
  $view = $variables['view'];
  if (isset($view->element['#display_id'])) {
    switch ($view->element['#display_id']) {
      case 'news_search':
        break;

      case 'news_search_old':
        news_search_view($variables);
        break;

      case 'block_news':
        news_search_view($variables);
        break;

    }
  }
}
