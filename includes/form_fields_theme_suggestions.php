<?php

/**
 * @file
 * Functions to add additional form templates.
 */

/**
 * Implements hook_theme_suggestions_HOOK_alter().
 */
function ucisom_theme_suggestions_form_element_alter(&$suggestions, $variables) {
  array_unshift($suggestions, 'form_element__' . $variables['element']['#type']);
}

/**
 * Implements hook_preprocess_form_element().
 */
function ucisom_preprocess_form_element(&$variables) {
  $variables['label']['#__element_type'] = $variables['element']['#type'];
}

/**
 * Implements hook_theme_suggestions_HOOK_alter().
 */
function ucisom_theme_suggestions_form_element_label_alter(&$suggestions, $variables) {
  $suggestions[] = 'form_element_label__' . $variables['element']['#__element_type'];
}
