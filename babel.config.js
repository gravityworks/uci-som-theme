module.exports = (api) => {
  api.cache(true);

  const presets = [
    [
      '@babel/preset-env',
      {
        corejs: 3,
        useBuiltIns: false,
      },
    ],
    'minify',
  ];

  const plugins = [
    '@babel/plugin-syntax-class-properties',
    '@babel/plugin-proposal-class-properties',
  ];

  const comments = false;

  return { presets, comments, plugins };
};
