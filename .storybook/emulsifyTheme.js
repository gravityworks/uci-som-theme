// Documentation on theming Storybook: https://storybook.js.org/docs/configurations/theming/

import { create } from '@storybook/theming';

export default create({
  base: 'light',
  // Branding
  brandTitle: 'University of California at Irving',
  brandUrl: 'https://som.uci.edu',
  brandImage: 'logo.png',
});
