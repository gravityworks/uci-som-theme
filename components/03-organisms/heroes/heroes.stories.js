import React from 'react';
import { useEffect } from '@storybook/client-api';

// import '../../02-molecules/menus/menus';

import heroTemplate from './hero.twig';

import heroDivisionData from './hero-division.yml';
import heroInteriorData from './hero-interior.yml';
// import heroBlueData from './hero-blue.yml';
// import heroShortData from './hero-short.yml';
// import heroDetailsData from './hero-division.yml';
import heroMicrositeData from './hero-microsite.yml';
import heroVideoData from './hero-video.yml';

export default { title: 'Organisms/Heroes' };

export const division = () => {
  useEffect(() => Drupal.attachBehaviors(), []);
  return (
    <div dangerouslySetInnerHTML={{ __html: heroTemplate(heroDivisionData) }} />
  );
};

export const microsite = () => (
  <div dangerouslySetInnerHTML={{ __html: heroTemplate(heroMicrositeData) }} />
);

export const interior = () => (
  <div dangerouslySetInnerHTML={{ __html: heroTemplate(heroInteriorData) }} />
);

// export const short = () => (
//   <div dangerouslySetInnerHTML={{ __html: heroTemplate(heroShortData) }} />
// );

// export const blue = () => (
//   <div dangerouslySetInnerHTML={{ __html: heroTemplate(heroBlueData) }} />
// );

// export const details = () => (
//   <div dangerouslySetInnerHTML={{ __html: heroTemplate(heroDetailsData) }} />
// );

export const heroVideo = () => (
  <div dangerouslySetInnerHTML={{ __html: heroTemplate(heroVideoData) }} />
);
