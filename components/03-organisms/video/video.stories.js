import React from 'react';

import playlistTemplate from './playlist.twig';
import playlistData from './playlist.yml';

import playerTemplate from './video_player_full.twig';
import playerData from './youtube-video.yml';
import playerDataNoGradient from './youtube-video-no-gradient.yml';

export default { title: 'Organisms/Video' };

export const playList = () => (
  <div dangerouslySetInnerHTML={{ __html: playlistTemplate(playlistData) }} />
);

export const videoPlayerFull = () => (
  <div dangerouslySetInnerHTML={{ __html: playerTemplate(playerData) }} />
);

export const videoPlayerFullNoGradient = () => (
  <div
    dangerouslySetInnerHTML={{ __html: playerTemplate(playerDataNoGradient) }}
  />
);
