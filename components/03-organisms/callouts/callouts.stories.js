import React from 'react';

import calloutsTemplate from './callouts.twig';
import miniMenuTemplate from './miniMenu/miniMenu.twig';

import miniMenuData from './miniMenu/miniMenu.yml';
import miniMenuNoBarTitleData from './miniMenu/miniMenu_noBarTitle.yml';
import searchData from './search/search.yml';

export default { title: 'Organisms/Callouts' };

export const miniMenu = () => (
  <>
    <div dangerouslySetInnerHTML={{ __html: miniMenuTemplate(miniMenuData) }} />
    <br />
    <div
      dangerouslySetInnerHTML={{
        __html: miniMenuTemplate(miniMenuNoBarTitleData),
      }}
    />
  </>
);

export const search = () => (
  <div dangerouslySetInnerHTML={{ __html: calloutsTemplate(searchData) }} />
);
