import React from 'react';

import footerTemplate from './_footer.twig';
import footerData from './footer.yml';

export default { title: 'Organisms/Footer' };

export const footer = () => (
  <div dangerouslySetInnerHTML={{ __html: footerTemplate(footerData) }} />
);
