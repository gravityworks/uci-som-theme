import React from 'react';

import error404Template from './404.twig';

export default { title: 'Organisms/Error' };

export const fourOFour = () => (
  <div dangerouslySetInnerHTML={{ __html: error404Template() }} />
);
