import React from 'react';
import { useEffect } from '@storybook/client-api';

import accordionTemplate from './accordion.twig';

import accordionData from './accordion.yml';

import './accordion';

export default { title: 'Organisms/Accordion' };

export const accordion = () => {
  useEffect(() => Drupal.attachBehaviors(), []);
  return (
    <div
      dangerouslySetInnerHTML={{ __html: accordionTemplate(accordionData) }}
    />
  );
};
