class Accordion {
  constructor(accordion) {
    this.accordion = accordion;
    accordion.addEventListener('click', this.onAccordionClick);
  }

  onAccordionClick = (event) => {
    if (event.currentTarget.getAttribute('aria-expanded') === 'true') {
      this.collapseSection(event.currentTarget);
    } else {
      this.expandSection(event.currentTarget);
    }
  };

  expandSection = (section) => {
    section.setAttribute('aria-expanded', true);
  };

  collapseSection = (section) => {
    section.setAttribute('aria-expanded', false);
  };
}

Drupal.behaviors.accordion = {
  attach(context) {
    if (context == document) {
      const accordions = context.querySelectorAll('.accordion-item');
      accordions.forEach((accordion) => new Accordion(accordion));
    }
  },
};
