import React from 'react';

import sidebarTemplate from './sidebar.twig';

import sidebarData from './sidebar.yml';
import eventData from '../../02-molecules/events/event.yml';

export default { title: 'Organisms/Sidebars' };

export const sidebar = () => (
  <div dangerouslySetInnerHTML={{ __html: sidebarTemplate(sidebarData) }} />
);

export const sidebarEventRegistration = () => (
  <div dangerouslySetInnerHTML={{ __html: sidebarTemplate(eventData) }} />
);
