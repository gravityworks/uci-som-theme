import React from 'react';

import imageTemplate from './image--full-width.twig';

import imageData from './image--full-width.yml';

export default { title: 'Organisms/Image' };

export const imageFullWidth = () => (
  <div dangerouslySetInnerHTML={{ __html: imageTemplate(imageData) }} />
);
