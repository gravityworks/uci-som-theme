/*  eslint-disable */
class Carousel {
  constructor(el) {
    this.wrapper = el;
    this.currentSlideIndex = 1;
    this.slides = this.wrapper.querySelectorAll('.carousel-slide');
    this.numSlides = this.slides.length;
    this.navItems = this.wrapper.querySelectorAll('.carousel-nav li');
    this.arrows = this.wrapper.querySelectorAll('.carousel-arrow');
    this.arrowPrev = this.wrapper.querySelector('.carousel-arrow-prev');
    this.arrowNext = this.wrapper.querySelector('.carousel-arrow-next');
    this.slideWrapper = this.wrapper.querySelector('.slide-wrapper');
    this.slideWidth = this.slideWrapper.offsetWidth;
    this.initNav();
    this.initArrows();
  }

  initNav = () => {
    window.addEventListener('resize', (e) => {
      this.calculateSlideWidth();
    });
    this.navItems.forEach((item) => {
      item.addEventListener('click', this.onNavItemClick);
    });
    this.updateNav();
  };

  initArrows = () => {
    this.arrows.forEach((arrow) => {
      arrow.addEventListener('click', this.onArrowClick);
    });
    this.updateArrows();
  };

  calculateSlideWidth = () => {
    const wrapper = this.wrapper.querySelector('.slide-wrapper');
    console.log(wrapper.offsetWidth);
    return wrapper.offsetWidth;
  };

  onNavItemClick = (e) => {
    this.setCurrentSlide(parseInt(e.currentTarget.dataset.buttonIndex, 10));
  };

  onArrowClick = (e) => {
    const data = e.currentTarget.dataset;
    const direction = data.direction;
    if (direction === 'next') {
      this.setCurrentSlide(this.currentSlideIndex + 1);
    } else if (direction === 'prev') {
      this.setCurrentSlide(this.currentSlideIndex - 1);
    }
  };

  setCurrentSlide = (newSlideIndex) => {
    if (newSlideIndex > 0 && newSlideIndex <= this.numSlides) {
      this.currentSlideIndex = newSlideIndex;
      this.goToSlide();
      this.updateArrows();
      this.updateNav();
    }
  };

  disableButton = (button) => {
    button.disabled = true;
  };

  updateNav = () => {
    this.navItems.forEach((item) => {
      if (item.dataset.buttonIndex == this.currentSlideIndex) {
        item.classList.add('active');
      } else {
        item.classList.remove('active');
      }
    });
  };

  updateArrows = () => {
    this.arrows.forEach((arrow) => {
      arrow.disabled = false;
    });
    if (this.currentSlideIndex <= 1) {
      this.disableButton(this.arrowPrev);
    } else if (this.currentSlideIndex >= this.numSlides) {
      this.disableButton(this.arrowNext);
    }
  };

  goToSlide = () => {
    const carouselPosition =
      -1 * ((this.currentSlideIndex - 1) * this.calculateSlideWidth());
    this.slideWrapper.setAttribute(
      'style',
      `transform: translateX(${carouselPosition}px)`,
    );
  };
}

Drupal.behaviors.carousel = {
  attach(context) {
    if (context == document) {
      const carouselElements = context.querySelectorAll('.carousel');
      carouselElements.forEach((carousel) => new Carousel(carousel));
    }
  },
};
