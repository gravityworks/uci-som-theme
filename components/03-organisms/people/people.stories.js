import React from 'react';

import peopleTeasersTemplate from './people-teasers.twig';

import peopleTeasersData from './people.yml';

export default { title: 'Organisms/People' };

export const peopleTeasersGrid = () => (
  <div
    dangerouslySetInnerHTML={{
      __html: peopleTeasersTemplate(peopleTeasersData),
    }}
  />
);
