import React from 'react';

import newsTeasersTemplate from './news/news-teasers.twig';
import pressReleaseTemplate from './pressRelease/pressRelease.twig';
import featuresBriefsTemplate from './features-briefs/features-briefs.twig';

// import newsItemsTeaserData from './news/news-items--teasers.yml';
// import pressReleaseData from './pressRelease/pressRelease.yml';
import pressReleaseData from '../../02-molecules/news/news.yml';
// import featuresBriefsData from './features-briefs/features-briefs.yml';

import newsData from './news.yml';

export default { title: 'Organisms/News' };

export const featuresBriefsTeaser = () => (
  <div
    dangerouslySetInnerHTML={{
      __html: featuresBriefsTemplate(newsData),
    }}
  />
);

export const newsAndPressReleaseTeasers = () => (
  <div
    dangerouslySetInnerHTML={{
      __html: newsTeasersTemplate(newsData),
    }}
  />
);

export const pressRelease = () => (
  <div
    dangerouslySetInnerHTML={{ __html: pressReleaseTemplate(pressReleaseData) }}
  />
);
