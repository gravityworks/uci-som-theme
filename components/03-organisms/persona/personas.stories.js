import React from 'react';

import personasTemplate from './personas.twig';

import personaData from './persona_items.yml';

import './personas';

export default { title: 'Organisms/Personas' };

export const panels = () => (
  <div dangerouslySetInnerHTML={{ __html: personasTemplate(personaData) }} />
);
