class Personas {
  constructor(el) {
    this.personas = el;
    this.personaPanels = el.querySelector('.persona-panels');
    this.personaMenuItems = el.querySelectorAll('.persona--menu-item');
    this.personaPanelItems = el.querySelectorAll('.persona-wrapper');
    this.init();
  }

  init = () => {
    this.personaMenuItems.forEach((personaMenuItem) =>
      personaMenuItem.addEventListener('click', this.onMenuItemClick),
    );
    this.personaPanelItems.forEach((personaItem) => {
      // console.log(personaItem);
      personaItem.addEventListener('click', this.buttonTest);
      const closeButton = personaItem.querySelector('.iconButton');
      const personaTab = personaItem.querySelector('.persona--tab');
      // console.log(personaTab);
      personaTab.addEventListener('click', this.onPersonaItemClick);
      closeButton.addEventListener('click', this.onCloseButtonClick);
    });
  };

  onPersonaItemClick = (e) => {
    // console.log(e.target);
    // console.log(e.currentTarget.parentNode);
    this.openPanel(e.currentTarget.parentNode);
  };

  closePanel = (personaItem) => {
    personaItem.classList.remove('active');
  };

  closeAllPanels = () => {
    this.personaPanelItems.forEach((personaItem) =>
      this.closePanel(personaItem),
    );
  };

  openPanel = (personaItem) => {
    this.hideMenu();
    this.closeAllPanels();
    personaItem.classList.add('active');
  };

  onCloseButtonClick = (e) => {
    // this.personaPanelItems.forEach((personaItem) =>
    //   this.closePanel(personaItem),
    // );
    this.closeAllPanels();
    this.showMenu();
  };

  onMenuItemClick = (e) => {
    e.preventDefault();
    this.personaPanelItems.forEach((item) => {
      if (item.dataset.persona == e.currentTarget.dataset.persona) {
        this.openPanel(item);
        return;
      }
    });
  };

  hideMenu = () => {
    this.personas.classList.add('hide-menu');
  };

  showMenu = () => {
    this.personas.classList.remove('hide-menu');
  };
}

Drupal.behaviors.personas = {
  attach(context) {
    if (context == document) {
      const items = context.querySelectorAll('.personas');
      items.forEach((personas) => new Personas(personas));
    }
  },
};
