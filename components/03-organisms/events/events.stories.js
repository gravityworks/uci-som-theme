import React from 'react';

import eventsTeasersTemplate from './_events-teasers.twig';
import eventsTeasersImageTemplate from './events-teasers-image.twig';
import eventsListTemplate from './events-list.twig';
import eventSearchTemplate from './events-search.twig';

import eventsData from './events.yml';

export default { title: 'Organisms/Events' };

export const eventsTeasers = () => (
  <div
    dangerouslySetInnerHTML={{ __html: eventsTeasersTemplate(eventsData) }}
  />
);

export const eventsTeasersImage = () => (
  <div
    dangerouslySetInnerHTML={{ __html: eventsTeasersImageTemplate(eventsData) }}
  />
);

export const eventsList = () => (
  <div dangerouslySetInnerHTML={{ __html: eventsListTemplate(eventsData) }} />
);

export const eventSearch = () => (
  <div dangerouslySetInnerHTML={{ __html: eventSearchTemplate(eventsData) }} />
);
