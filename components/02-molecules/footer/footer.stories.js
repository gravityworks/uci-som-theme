import React from 'react';

import registrationFormTemplate from './registration-form.twig';
import footerInfoTemplate from './footer-info.twig';
import footerLinksTemplate from './footer-links.twig';
import footerBarTemplate from './footer-bar.twig';

import footerData from '../../03-organisms/footer/footer.yml';

import footerInfoData from './footer-info.yml';
import footerLinksData from './footer-links.yml';
import footerBarData from './footer-bar.yml';

export default { title: 'Molecules/Footer' };

export const footerRegistration = () => (
  <footer>
    <div
      className="sb-footer"
      dangerouslySetInnerHTML={{
        __html: registrationFormTemplate(footerData),
      }}
    />
  </footer>
);

export const footerInfo = () => (
  <footer>
    <div
      className="sb-footer"
      dangerouslySetInnerHTML={{
        __html: footerInfoTemplate(footerInfoData),
      }}
    />
  </footer>
);

export const footerLinks = () => (
  <footer>
    <div
      className="sb-footer"
      dangerouslySetInnerHTML={{ __html: footerLinksTemplate(footerLinksData) }}
    />
  </footer>
);

export const footerBar = () => (
  <footer>
    <div
      dangerouslySetInnerHTML={{ __html: footerBarTemplate(footerBarData) }}
    />
  </footer>
);
