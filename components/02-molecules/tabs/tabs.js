class Tabs {
  constructor(tabs) {
    this.tabs = tabs;
    this.tabsContainers = document.querySelectorAll('.js-tabs-container');
    tabs.forEach((tab) => {
      tab.addEventListener('click', this.onTabClick);
    });
    this.init();
  }

  init = () => {
    this.activateTab(this.tabs[0]);
    this.activateTabContainer(this.tabsContainers[0]);
    console.log(this.tabsContainers[0]);
  };

  onTabClick = (event) => {
    this.activateTab(event.currentTarget);
    this.activateTabContainer(
      document.querySelector(
        `.js-tabs-container.${event.currentTarget.dataset.tabId}`,
      ),
    );
  };

  activateTab = (tab) => {
    this.tabs.forEach((tab) => {
      tab.classList.remove('button-tab--active');
    });
    tab.classList.add('button-tab--active');
  };

  activateTabContainer = (container) => {
    this.tabsContainers.forEach((container) =>
      container.classList.remove('active'),
    );
    container.classList.add('active');
  };
}

Drupal.behaviors.tabs = {
  attach(context) {
    if (context == document) {
      const tabsContainer = document.querySelector('.tabs');
      if (tabsContainer.classList.contains('no-js')) {
        return;
      } else {
        const tabs = new Tabs(context.querySelectorAll('.button-tab'));
      }
    }
  },
};
