import React from 'react';

import tabsTemplate from './tabs.twig';
import tabsData from './tabs.yml';

export default { title: 'Molecules/Tabs' };

export const tabs = () => (
  <div dangerouslySetInnerHTML={{ __html: tabsTemplate(tabsData) }} />
);
