import React from 'react';

import accordionItemTemplate from './accordion_item.twig';
import accordionItemData from './accordion_item.yml';

export default { title: 'Molecules/Accordion Item' };

export const accordionBar = () => (
  <div
    dangerouslySetInnerHTML={{
      __html: accordionItemTemplate(accordionItemData),
    }}
  />
);
