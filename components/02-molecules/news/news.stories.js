import React from 'react';

import newsTeaserTemplate from '../news-events/teaser.twig';
import newsCardTemplate from '../card/card.twig';
import newsSocialTemplate from './social/news-social.twig';
import relatedFacultyTemplate from './_related-faculty.twig';
import relatedLinksTemplate from './related-links.twig';

import newsData from './news.yml';
import newsSocialData from './social/departments.yml';
import relatedLinksData from './related-links.yml';
// import newsFeatureBriefData from './feature-brief.yml';
// import relatedFacultyData from './relat'

export default { title: 'Molecules/News' };

export const featureBrief = () => (
  <div
    className="sb-teaser"
    dangerouslySetInnerHTML={{
      __html: newsCardTemplate(newsData),
    }}
  />
);

export const teaserFeatured = () => (
  <div
    className="sg-newsTeaserFeatured sb-teaser"
    dangerouslySetInnerHTML={{
      __html: newsCardTemplate(newsData),
    }}
  />
);

export const teaserNews = () => (
  <div
    dangerouslySetInnerHTML={{
      __html: newsTeaserTemplate(newsData),
    }}
  />
);

export const social = () => (
  <div
    dangerouslySetInnerHTML={{ __html: newsSocialTemplate(newsSocialData) }}
  />
);

export const relatedFaculty = () => (
  <div
    dangerouslySetInnerHTML={{
      __html: relatedFacultyTemplate(newsData),
    }}
  />
);

export const relatedLinks = () => (
  <div
    dangerouslySetInnerHTML={{ __html: relatedLinksTemplate(relatedLinksData) }}
  />
);
