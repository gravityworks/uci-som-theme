import React from 'react';

import eventSearchTemplate from './event-search.twig';
import eventSearchData from './event-search.yml';

import monthPaginationTemplate from './month-pagination.twig';
import monthPaginationData from './month-pagination.yml';

export default { title: 'Molecules/Forms' };

export const eventSearch = () => (
  <div
    dangerouslySetInnerHTML={{ __html: eventSearchTemplate(eventSearchData) }}
  />
);

export const monthPagination = () => (
  <div
    dangerouslySetInnerHTML={{
      __html: monthPaginationTemplate(monthPaginationData),
    }}
  />
);
