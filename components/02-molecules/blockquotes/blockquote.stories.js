import React from 'react';

import blockquotes from './blockquote.twig';

import blockquoteData from './yml/blockquote.yml';
import blockquoteImageData from './yml/blockquote-image.yml';
import blockquoteImageRightData from './yml/blockquote-image-right.yml';
import blockquoteDarkData from './yml/blockquote-dark.yml';
import blockquoteDarkImageData from './yml/blockquote-dark-image.yml';
import blockquoteYellowData from './yml/blockquote-yellow.yml';
import blockquoteYellowImageData from './yml/blockquote-yellow-image.yml';

/**
 *  Storybook Definition.
 */
export default { title: 'Molecules/Blockquotes' };

export const blockquote = () => (
  <div dangerouslySetInnerHTML={{ __html: blockquotes(blockquoteData) }} />
);

export const blockquoteWithImage = () => (
  <div dangerouslySetInnerHTML={{ __html: blockquotes(blockquoteImageData) }} />
);

export const blockquoteWithImageRight = () => (
  <div
    dangerouslySetInnerHTML={{ __html: blockquotes(blockquoteImageRightData) }}
  />
);

export const blockquoteDark = () => (
  <div dangerouslySetInnerHTML={{ __html: blockquotes(blockquoteDarkData) }} />
);

export const blockquoteDarkWithImage = () => (
  <div
    dangerouslySetInnerHTML={{ __html: blockquotes(blockquoteDarkImageData) }}
  />
);

export const blockquoteYellow = () => (
  <div
    dangerouslySetInnerHTML={{ __html: blockquotes(blockquoteYellowData) }}
  />
);

export const blockquoteYellowWithImage = () => (
  <div
    dangerouslySetInnerHTML={{ __html: blockquotes(blockquoteYellowImageData) }}
  />
);
