import React from 'react';

import socialLinksTemplate from './socialLinks.twig';

import socialLinksData from './socialLinks.yml';
import socialLinksWhiteData from './socialLinks--white.yml';
import socialLinksBlueData from './socialLinks--blue.yml';
import socialLinksBlueAltData from './socialLinks--blue-alt.yml';

export default { title: 'Molecules/Social Links' };

export const socialLinks = () => (
  <div
    className="sg-socialLinks"
    dangerouslySetInnerHTML={{ __html: socialLinksTemplate(socialLinksData) }}
  />
);

export const socialLinksWhite = () => (
  <div
    className="sg-socialLinks"
    dangerouslySetInnerHTML={{
      __html: socialLinksTemplate(socialLinksWhiteData),
    }}
  />
);

export const socialLinksBlue = () => (
  <div
    className="sg-socialLinks blue"
    dangerouslySetInnerHTML={{
      __html: socialLinksTemplate(socialLinksBlueData),
    }}
  />
);

export const socialLinksBlueAlt = () => (
  <div
    className="sg-socialLinks blue"
    dangerouslySetInnerHTML={{
      __html: socialLinksTemplate(socialLinksBlueAltData),
    }}
  />
);
