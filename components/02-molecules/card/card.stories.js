import React from 'react';
// import { useEffect } from '@storybook/client-api';

import cardTemplate from './card.twig';
import newsData from '../news/news.yml';
import topicData from './topic.yml';
import departmentData from './department.yml';

export default { title: 'Molecules/Card' };

export const topic = () => (
  <div dangerouslySetInnerHTML={{ __html: cardTemplate(topicData) }} />
);

export const featuredNews = () => (
  <div dangerouslySetInnerHTML={{ __html: cardTemplate(newsData) }} />
);

export const department = () => (
  <div dangerouslySetInnerHTML={{ __html: cardTemplate(departmentData) }} />
);
