class JumpLinkMenu {
  constructor(menu) {
    this.menu = menu;
    this.container = menu.querySelector('.buttons-wrapper');
    this.sections = document.querySelectorAll('[data-jumpid]');
    this.init();
    // console.log(this.sections);
    //   console.log(document);
    //   console.log(this.container);
  }

  init = () => {
    [...this.sections].map((section) => {
      this.addButton(section.textContent, section.dataset.jumpid);
    });
    let section = window.location.search.split('?')[1];
    section = section.split('&');
    section = section.find((string) => string.startsWith('section'));
    section = section.split('=')[1];
    this.scrollTo(this.getTarget(section));
  };

  addButton = (name, id) => {
    const button = document.createElement('a');
    button.innerHTML = name;
    button.setAttribute('href', `?section=${id}`);
    button.setAttribute('class', 'button');
    button.setAttribute('data-targetid', id);
    button.addEventListener('click', this.onButtonClick);
    this.container.appendChild(button);
  };

  onButtonClick = (e) => {
    e.preventDefault();
    console.log(e.currentTarget.dataset);
    const targetId = e.currentTarget.dataset.targetid;
    const target = this.getTarget(targetId);
    window.history.pushState(null, null, `?section=${targetId}`);
    this.scrollTo(target);
  };

  getTarget = (id) => {
    return document.querySelector(`[data-jumpid=${id}]`);
  };

  scrollTo = (target) => {
    target.scrollIntoView({ behavior: 'smooth' });
  };
}

Drupal.behaviors.jumpLinkMenu = {
  attach(context) {
    if (context == document) {
      const jumpLinkMenu = context.querySelectorAll('.jump-link-menu');
      jumpLinkMenu.forEach((menu) => new JumpLinkMenu(menu));
    }
  },
};
