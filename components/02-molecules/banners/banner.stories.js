import React from 'react';

import banners from './banner.twig';

import bannerBlueData from './banner-blue.yml';
import bannerWhiteData from './banner-white.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Banners' };

export const bannerBlue = () => (
  <div dangerouslySetInnerHTML={{ __html: banners(bannerBlueData) }} />
);

export const bannerWhite = () => (
  <div dangerouslySetInnerHTML={{ __html: banners(bannerWhiteData) }} />
);
