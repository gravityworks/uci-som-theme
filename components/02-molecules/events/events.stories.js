import React from 'react';

import teaserTemplate from '../news-events/teaser.twig';
import eventDetailTemplate from './event_details.twig';
import speakerTemplate from './event_speakers.twig';
import sponsorsTemplate from './event_sponsors.twig';
import agendaItemTemplate from './event_agenda.twig';
import eventListTemplate from './event_list.twig';

import eventData from './event.yml';

export default { title: 'Molecules/Events' };

export const teaser = () => (
  <div
    dangerouslySetInnerHTML={{
      __html: teaserTemplate(eventData),
    }}
  />
);

export const detail = () => (
  <div dangerouslySetInnerHTML={{ __html: eventDetailTemplate(eventData) }} />
);

export const speakers = () => (
  <div dangerouslySetInnerHTML={{ __html: speakerTemplate(eventData) }} />
);

export const sponsors = () => (
  <div dangerouslySetInnerHTML={{ __html: sponsorsTemplate(eventData) }} />
);

export const agendaItem = () => (
  <div dangerouslySetInnerHTML={{ __html: agendaItemTemplate(eventData) }} />
);

export const listItem = () => (
  <div dangerouslySetInnerHTML={{ __html: eventListTemplate(eventData) }} />
);
