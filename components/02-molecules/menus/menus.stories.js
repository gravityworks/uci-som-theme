import React from 'react';

import menuPrimaryTemplate from './menu-primary.twig';
import menuPrimaryData from './menu-primary.yml';
import menuPrimaryDataChao from './menu-primary-chao.yml';

import menuPrimarySubNavTemplate from './_collapsing_nav.twig';
import menuPrimarySubNavData from './subnav.yml';

import menuSecondaryTemplate from './menu-secondary.twig';
import menuSecondaryData from './menu-secondary.yml';

import menuGlobalTemplate from './menu-global.twig';
import menuGlobalData from './menu-global.yml';

import './menus';
import './main-menu';

export default { title: 'Molecules/Menus' };

export const primaryMenu = () => (
  <>
    <div
      dangerouslySetInnerHTML={{ __html: menuPrimaryTemplate(menuPrimaryData) }}
    />
    <br />
    <div
      dangerouslySetInnerHTML={{
        __html: menuPrimaryTemplate(menuPrimaryDataChao),
      }}
    />
  </>
);

export const subNav = () => (
  <div
    dangerouslySetInnerHTML={{
      __html: menuPrimarySubNavTemplate(menuPrimarySubNavData),
    }}
  />
);

export const secondaryMenu = () => (
  <div
    dangerouslySetInnerHTML={{
      __html: menuSecondaryTemplate(menuSecondaryData),
    }}
  />
);

export const globalMenu = () => (
  <div
    dangerouslySetInnerHTML={{ __html: menuGlobalTemplate(menuGlobalData) }}
  />
);
