// Drupal.behaviors.mobileMenu = {
//   attach(context) {
//     console.log('bubu');
//   },
// };

const threshold = 100;
const fixed_nav = document.querySelector('.fixed-nav');
const main_menu = document.querySelector('.main-menu');
let prevScrollPos = window.pageYOffset;
window.onscroll = function () {
  const currentScrollPos = window.pageYOffset;
  if (prevScrollPos > currentScrollPos) {
    fixed_nav.classList.add('show');
  } else if (
    currentScrollPos > threshold &&
    !main_menu.classList.contains('active')
  ) {
    fixed_nav.classList.remove('show');
  }
  prevScrollPos = currentScrollPos;
};

// Make sure the 'active' class is removed from the secondary menu on page load
document.addEventListener('DOMContentLoaded', () => {
  const menuSecondary = document.querySelector('nav.menu_secondary');
  menuSecondary.classList.remove('show');
});

// Secondary menu show/hide
const menuSecondary3 = document.querySelector('.menu_secondary');
let prevScrollPos3 = window.pageYOffset;

window.addEventListener('scroll', () => {
  const currentScrollPos3 = window.pageYOffset;

  if (prevScrollPos3 > currentScrollPos3) {
    // Scrolled up
    menuSecondary3.classList.remove('show');
    document.querySelector('.menu_secondary').classList.remove('active');
  } else {
    // Scrolled down
    if (currentScrollPos3 >= 100) {
      menuSecondary3.classList.add('show');
    }
  }

  prevScrollPos3 = currentScrollPos3;
});

// Hide the secondary menu if clicked outside of itself
document.addEventListener('click', function(event) {
  if (!event.target.closest('nav.menu_secondary')) {
    const activeElement = document.querySelector('nav.menu_secondary.active');

    if (activeElement) {
      activeElement.classList.remove('active');
    }
  }
});

// Hide the secondary menu when Esc key is clicked
document.addEventListener('keydown', function(event) {
  const menuSecondary = document.querySelector('nav.menu_secondary');

  if ((event.key === 'Escape') && menuSecondary.classList.contains('active')) {
    menuSecondary.classList.remove('active');
  }
});