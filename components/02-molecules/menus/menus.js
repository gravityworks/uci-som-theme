class MicrositeMenu {
  constructor(component) {
    this.component = component;
    this.menuButton = this.component.querySelector('button');
    this.menuButton.addEventListener('click', this.onMenuButtonClick);
  }
  onMenuButtonClick = (e) => {
    console.log('click', e.currentTarget.parentNode.parentNode);
    const navBar = e.currentTarget.parentNode.parentNode.parentNode;
    navBar.classList.toggle('active');
  };
}

Drupal.behaviors.menus = {
  attach(context) {
    if (context == document) {
      const el = context.querySelectorAll('.menu_secondary');
      el.forEach((el) => new MicrositeMenu(el));
    }
  },
};
