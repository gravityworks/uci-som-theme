class CollapsingNav {
  constructor(component) {
    this.component = component;
    this.init();
  }

  init = () => {
    this.component.addEventListener('click', this.onMenuToggleClick);
  };

  onMenuToggleClick = (e) => {
    e.stopPropagation();
    const nav = e.currentTarget.parentNode.parentNode.parentNode;
    if (nav.classList.contains('expanded')) {
      nav.classList.remove('expanded');
      return;
    } else {
      // this.closeSubMenus();
      nav.classList.add('expanded');
    }
  };
}

Drupal.behaviors.collapsing_nav = {
  attach(context) {
    if (context == document) {
      const navs = context.querySelectorAll('.menu-arrow');
      navs.forEach((nav) => new CollapsingNav(nav));
    }
  },
};
