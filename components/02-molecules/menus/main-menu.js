class MainMenu {
  constructor(component) {
    this.component = component;
    this.mainMenu = this.component.querySelector('.main-menu');
    this.mainMenuItems = this.component.querySelectorAll('.main-menu--item');
    this.mainMenuToggles = this.component.querySelectorAll('.mobile-expand');
    this.mainMenuTopLevelLinks = this.component.querySelectorAll(
      '.main-menu--link',
    );
    this.menuButton = this.component.querySelector('.menu-button');
    this.collapsingNavItems = this.component.querySelectorAll('.menu-arrow');
    this.initMainMenuToggles();
    // this.initCollapsingNav();
    this.initMobileButton();
    this.initMainMenuTopLinks();
    document.querySelector('body').addEventListener('click', this.onBodyClick);
    // console.log(this.component.querySelectorAll('.mobile-expand'));
  }

  // onMenuToggleClick = (e) => {
  //   const nav = e.currentTarget.parentNode.parentNode.parentNode;
  //   if (nav.classList.contains('expanded')) {
  //     nav.classList.remove('expanded');
  //     return;
  //   } else {
  //     // this.closeSubMenus();
  //     nav.classList.add('expanded');
  //   }
  // };

  onBodyClick = (e) => {
    if (!this.component.contains(e.target)) {
      this.closeMenuItems();
    }
  };

  initMobileButton = () => {
    this.menuButton.addEventListener('click', this.onMenuButtonClick);
  };

  initMainMenuTopLinks = () => {
    this.mainMenuTopLevelLinks.forEach((item) => {
      item.addEventListener('click', this.onMainMenuToggleClick);
    });
  };

  onMenuButtonClick = (e) => {
    console.log('onMenuButtonClick');
    this.menuButton.classList.toggle('active');
    this.mainMenu.classList.toggle('active');
  };

  // initCollapsingNav = () => {
  //   this.collapsingNavItems.forEach((navItem) => {
  //     navItem.addEventListener('click', this.onMenuToggleClick);
  //   });
  // };

  onMainMenuToggleClick = (e) => {
    // const navItem = e.currentTarget;
    // navItem.classList.toggle('open');
    e.stopPropagation();
    this.closeSubMenus();

    // console.log('click', e.currentTarget.dataset.href, e.target, screen.width);
    if (e.currentTarget.dataset.href) {
      if (window.innerWidth > 1080) {
        const navItem = e.currentTarget.parentNode.parentNode;
        this.toggleMenu(navItem);
      } else {
        document.location.href = e.currentTarget.dataset.href;
      }
    } else {
      const navItem = e.currentTarget.parentNode.parentNode;
      this.toggleMenu(navItem);
    }
  };

  toggleMenu = (navItem) => {
    if (navItem.classList.contains('open')) {
      this.closeMainMenu(navItem);
    } else {
      this.openMainMenu(navItem);
    }
  };

  openMainMenu = (menu) => {
    this.mainMenuItems.forEach((menuItem) => {
      menuItem.classList.remove('open');
    });
    menu.classList.add('open');
  };

  closeMainMenu = (menu) => {
    menu.classList.remove('open');
  };

  initMainMenuToggles = () => {
    this.mainMenuToggles.forEach((toggle) => {
      toggle.addEventListener('click', this.onMainMenuToggleClick);
    });
  };

  closeMenuItems = () => {
    this.mainMenuItems.forEach((item) => {
      item.classList.remove('open');
    });
  };

  closeSubMenus = () => {
    this.collapsingNavItems.forEach((toggle) => {
      const nav = toggle.parentNode.parentNode.parentNode;
      nav.classList.remove('expanded');
    });
  };
}

Drupal.behaviors.menu = {
  attach(context) {
    if (context == document) {
      const mainMenu = context.querySelectorAll('.menu_primary');
      mainMenu.forEach((menu) => new MainMenu(menu));
    }
  },
};
