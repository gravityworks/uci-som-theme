class NavSearchBar {
  constructor(component) {
    this.component = component;
    this.trigger = component.querySelector('.trigger');
    this.searchField = component.querySelector('#search-term');
    this.form = component.querySelector('.form');
    if (this.trigger) {
      this.initTrigger();
    }
    this.form.addEventListener('submit', this.onFormSubmit);
  }

  initTrigger = () => {
    this.trigger.addEventListener('click', this.onTriggerClick);
  };

  onTriggerClick = (e) => {
    e.currentTarget.parentNode.classList.toggle('exposed');
  };

  onFormSubmit = (e) => {
    e.preventDefault();
    console.log(this.searchField.value);
    document.location.href = `/search?search=${this.searchField.value}`;
  };
}

Drupal.behaviors.navSearchBar = {
  attach(context) {
    if (context == document) {
      const searchBar = context.querySelectorAll('.nav-search');
      searchBar.forEach((bar) => new NavSearchBar(bar));
    }
  },
};
