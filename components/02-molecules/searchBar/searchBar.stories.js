import React from 'react';

import searchBarTemplate from './searchBar.twig';
import searchBarData from './searchBar.yml';
import searchBarMiniData from './searchBar--mini.yml';
import searchBarNavData from './searchBar--nav.yml';

export default { title: 'Molecules/Search Bar' };

export const searchBar = () => (
  <div dangerouslySetInnerHTML={{ __html: searchBarTemplate(searchBarData) }} />
);

export const searchBarMini = () => (
  <div
    className="sg-form--blue"
    dangerouslySetInnerHTML={{ __html: searchBarTemplate(searchBarMiniData) }}
  />
);

export const serachBarNav = () => (
  <div
    dangerouslySetInnerHTML={{ __html: searchBarTemplate(searchBarNavData) }}
  />
);
