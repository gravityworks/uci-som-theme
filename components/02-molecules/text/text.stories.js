import React from 'react';

import summaryTemplate from './summary/summary.twig';
import textWithImageTempalte from './image/textWithImage.twig';
import directoryTemplate from './directory/directory.twig';

import summaryData from './summary/summary.yml';
import summaryIconData from './summary/summary-icon.yml';
import summaryShareData from './summary/summary-share.yml';
import directoryData from './directory/directory.yml';

import textWithImageData from './image/textWithImage.yml';

export default { title: 'Molecules/Text' };

export const summary = () => (
  <div dangerouslySetInnerHTML={{ __html: summaryTemplate(summaryData) }} />
);

export const directory = () => (
  <div dangerouslySetInnerHTML={{ __html: directoryTemplate(directoryData) }} />
);

export const summaryIcon = () => (
  <div
    className="sb-summaryIcon"
    dangerouslySetInnerHTML={{ __html: summaryTemplate(summaryIconData) }}
  />
);

export const summaryShare = () => (
  <div
    dangerouslySetInnerHTML={{ __html: summaryTemplate(summaryShareData) }}
  />
);

export const textWithImage = () => (
  <div
    dangerouslySetInnerHTML={{
      __html: textWithImageTempalte(textWithImageData),
    }}
  />
);
