import React from 'react';
// import { useEffect } from '@storybook/client-api';

import playlistTemplate from './playlist-item/playlist_item.twig';
import htmlVideoTemplate from './html/html-video.twig';
import youtubeVideoTemplate from './youtube/youtube-video.twig';
import vimeoVideoTemplate from './vimeo/vimeo-video.twig';

import playlistData from './playlist-item/playlist_item.yml';
import htmlVideoData from './html/html-video.yml';
import youtubeVideoData from './youtube/youtube-video.yml';
import vimeoVideoData from './vimeo/vimeo-video.yml';
import './html/html-video';
// import './youtube-video';

export default { title: 'Molecules/Video' };

export const playlistItem = () => (
  <div dangerouslySetInnerHTML={{ __html: playlistTemplate(playlistData) }} />
);

export const htmlVideo = () => {
  // useEffect(() => Drupal.attachBehaviors(), []);
  return (
    <div
      dangerouslySetInnerHTML={{ __html: htmlVideoTemplate(htmlVideoData) }}
    />
  );
};

export const youtubeVideo = () => (
  <div
    dangerouslySetInnerHTML={{ __html: youtubeVideoTemplate(youtubeVideoData) }}
  />
);

export const vimeoVideo = () => (
  <div
    dangerouslySetInnerHTML={{ __html: vimeoVideoTemplate(vimeoVideoData) }}
  />
);
