class HTMLVideoPlayer {
  constructor(component) {
    this.component = component;
    this.videoPlayer = this.component.querySelector('video');
    this.buttons = component.querySelectorAll('button');
    this.playButton = component.querySelector('.play-btn');
    this.pauseButton = component.querySelector('.pause-btn');
    this.initButtons();
    this.initAutoPlay();
  }

  initButtons = () => {
    this.buttons.forEach((button) => {
      button.addEventListener('click', this.togglePlay);
    });
  };

  initAutoPlay = () => {
    if (window.innerWidth > 1080) {
      const potentialHero = this.videoPlayer.parentElement.parentElement;
      if (potentialHero.classList.contains('hero')) {
        potentialHero.classList.add('has-video');
      }
      this.videoPlayer.setAttribute('autoplay', '');
      this.videoPlayer.setAttribute('preload', 'true');
    }
  };

  togglePlay = () => {
    if (this.videoPlayer.paused) {
      this.playVideo();
    } else {
      this.pauseVideo();
    }
  };

  playVideo = () => {
    this.videoPlayer.play();
    this.playButton.classList.add('hidden');
    this.pauseButton.classList.remove('hidden');
  };

  pauseVideo = () => {
    this.videoPlayer.pause();
    this.playButton.classList.remove('hidden');
    this.pauseButton.classList.add('hidden');
  };
}

Drupal.behaviors.htmlVideoPlayer = {
  attach(context) {
    console.log(context);
    if (context == document) {
      const videoPlayers = context.querySelectorAll('.html-video');
      videoPlayers.forEach((videoPlayer) => new HTMLVideoPlayer(videoPlayer));
    }
  },
};
