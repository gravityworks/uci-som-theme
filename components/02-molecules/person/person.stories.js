import React from 'react';

import personTeaserTemplate from './person-teaser.twig';

import personTeaserData from './person-teaser.yml';

export default { title: 'Molecules/Person' };

export const personTeaser = () => (
  <div
    dangerouslySetInnerHTML={{ __html: personTeaserTemplate(personTeaserData) }}
  />
);
