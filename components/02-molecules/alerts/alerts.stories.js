import React from 'react';

import alertTemplate from '../accordion/accordion_item.twig';
import alertData from './alert.yml';

export default { title: 'Molecules/Alert' };

export const alert = () => (
  <div
    dangerouslySetInnerHTML={{
      __html: alertTemplate(alertData),
    }}
  />
);
