import React from 'react';

import menuItemTemplate from './menu_item.twig';
import tabTemplate from './tab.twig';
import panelTemplate from './panel.twig';

import personaItemData from './persona_item.yml';

export default { title: 'Molecules/Persona' };

export const menuItem = () => (
  <div
    dangerouslySetInnerHTML={{ __html: menuItemTemplate(personaItemData) }}
  />
);

export const tab = () => (
  <div dangerouslySetInnerHTML={{ __html: tabTemplate(personaItemData) }} />
);

export const panel = () => (
  <div dangerouslySetInnerHTML={{ __html: panelTemplate(personaItemData) }} />
);
