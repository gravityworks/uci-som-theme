import React from 'react';

import breadcrumb from './breadcrumb.twig';

import breadcrumbData from './breadcrumbs.yml';

/**
 * Stroybook Definition
 */

export default { title: 'Molecules/Breadcrumbs' };

export const breadcrumbs = () => (
  <div
    className="sb-breadcrumbs"
    dangerouslySetInnerHTML={{ __html: breadcrumb(breadcrumbData) }}
  />
);
