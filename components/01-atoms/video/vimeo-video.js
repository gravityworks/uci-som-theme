class VimeoVideoPlayer {
  constructor(component) {
    this.component = component;
    this.videoId = this.component.dataset.vimeoVideoId;
    this.modal = this.component.querySelector('.player-modal');
    this.playerContainer = this.component.querySelector('.player-container');
    this.isPlaying = false;
    this.player = null;
    this.closeBtn = this.component.querySelector('.close-btn');
    this.component.addEventListener('click', this.onVideoPlayerClick);
    this.closeBtn.addEventListener('click', this.onCloseBtnClick);
    this.loadVideo();
    // window.Vimeo.ready(this.loadVideo);
    // window.YT.ready(this.loadVideo);
    // console.log(window.location);
    // console.log('Vimeo Player');
  }

  loadVideo = () => {
    const videoDiv = document.createElement('div');
    videoDiv.classList = 'vimeo-embeded-video-player';
    let id = `vimeo-${this.videoId}`;
    videoDiv.setAttribute('id', id);
    this.modal.appendChild(videoDiv);
    this.player = new window.Vimeo.Player(id, {
      id: this.videoId,
      // responsive: true,
      // events: {
      //   onReady: this.onPlayerReady,
      //   onStateChange: this.onPlayerStateChange,
      // },
      origin: window.location.origin,
    });
  };

  onCloseBtnClick = (e) => {
    console.log(e);
    e.stopPropagation();
    e.preventDefault();
    this.closeModal();
  };

  onPlayerStateChange = (e) => {
    // console.log('onPlayerStateChange', e);
    if (e.data == 0) {
      this.closeModal();
    }
  };

  onVideoPlayerClick = (event) => {
    console.log('click', event.target);
    if (
      event.target.classList.contains('player-modal') ||
      event.target.classList.contains('player-container')
    ) {
      this.closeModal();
    } else {
      this.openModal();
    }
  };

  closeModal = () => {
    this.modal.classList.remove('visible');
    this.player.setCurrentTime(0);
    this.player.pause();
    // if (this.player.getPlayerState() == 0) {
    //   this.player.seekTo(0);
    //   this.player.pauseVideo();
    // } else {
    //   this.player.pauseVideo();
    // }
  };

  openModal = () => {
    this.modal.classList.add('visible');
    this.player.play();
  };
}

Drupal.behaviors.vimeoVideoPlayer = {
  attach(context) {
    const vimeoVideoPlayers = context.querySelectorAll('[data-vimeo-video-id]');
    vimeoVideoPlayers.forEach(
      (videoPlayer) => new VimeoVideoPlayer(videoPlayer),
    );
  },
};
