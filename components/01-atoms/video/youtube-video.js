class YoutubeVideoPlayer {
  constructor(component) {
    this.component = component;
    this.videoId = this.component.dataset.videoId;
    this.modal = this.component.querySelector('.player-modal');
    this.playerContainer = this.component.querySelector('.player-container');
    this.isPlaying = false;
    this.player = null;
    this.closeBtn = this.component.querySelector('.close-btn');
    window.YT.ready(this.loadVideo);
    console.log(window.location);
  }

  loadVideo = () => {
    const videoDiv = document.createElement('div');
    videoDiv.classList = 'youtube-embeded-video-player';
    const id = `yt-${this.videoId}`;
    videoDiv.setAttribute('id', id);
    this.playerContainer.appendChild(videoDiv);
    this.player = new window.YT.Player(id, {
      videoId: this.videoId,
      events: {
        onReady: this.onPlayerReady,
        onStateChange: this.onPlayerStateChange,
      },
      origin: window.location.origin,
    });
  };

  onPlayerReady = () => {
    this.component.addEventListener('click', this.onVideoPlayerClick);
    this.closeBtn.addEventListener('click', this.onCloseBtnClick);
  };

  onCloseBtnClick = (e) => {
    console.log(e);
    e.stopPropagation();
    e.preventDefault();
    this.closeModal();
  };

  onPlayerStateChange = (e) => {
    // console.log('onPlayerStateChange', e);
    if (e.data === 0) {
      this.closeModal();
    }
  };

  onVideoPlayerClick = (event) => {
    // console.log('click', e.target);
    if (event.target.classList.contains('player-modal')) {
      this.closeModal();
    } else {
      this.openModal();
    }
  };

  closeModal = () => {
    this.modal.classList.remove('visible');
    if (this.player.getPlayerState() === 0) {
      this.player.seekTo(0);
      this.player.pauseVideo();
    } else {
      this.player.pauseVideo();
    }
  };

  openModal = () => {
    this.modal.classList.add('visible');
    this.player.playVideo();
  };
}

Drupal.behaviors.youtubeVideoPlayer = {
  attach(context) {
    console.log('youtubeVideoPlayer');
    const youtubeVideoPlayers = context.querySelectorAll('[data-video-id]');
    youtubeVideoPlayers.forEach(
      (videoPlayer) => new YoutubeVideoPlayer(videoPlayer),
    );
  },
};
