import React from 'react';

import dl from './dl.twig';
import ul from './ul.twig';
import ol from './ol.twig';

import dlData from './dl.yml';
import ulData from './ul.yml';
import olData from './ol.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Atoms/Lists' };

export const definitionList = () => (
  <div className="sg-lists" dangerouslySetInnerHTML={{ __html: dl(dlData) }} />
);
export const unorderedList = () => (
  <div className="sg-lists" dangerouslySetInnerHTML={{ __html: ul(ulData) }} />
);
export const orderedList = () => (
  <div className="sg-lists" dangerouslySetInnerHTML={{ __html: ol(olData) }} />
);
