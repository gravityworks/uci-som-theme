import React from 'react';

import heading from './headings/_heading.twig';
import sectionHeader from './headings/_sectionHeader.twig';
import paragraph from './text/03-inline-elements.twig';

import headingData from './headings/headings.yml';
import sectionHeaderData from './headings/sectionHeaders.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Atoms/Text' };

// Loop over items in headingData to show each one in the example below.
const headings = headingData.map((d) => heading(d)).join('');
// Loop over items in headerData to show each one in the example below.
const sectionHeaders = sectionHeaderData.map((d) => sectionHeader(d)).join(' ');

export const headingsExamples = () => (
  <div dangerouslySetInnerHTML={{ __html: headings }} />
);

export const sectionHeaderExamples = () => (
  <div
    className="sg-sectionHeaderExamples"
    dangerouslySetInnerHTML={{ __html: sectionHeaders }}
  />
);

// export const blockquoteExample = () => (
//   <div dangerouslySetInnerHTML={{ __html: blockquote(blockquoteData) }} />
// );
// export const preformatted = () => (
//   <div dangerouslySetInnerHTML={{ __html: pre({}) }} />
// );
export const random = () => (
  <div dangerouslySetInnerHTML={{ __html: paragraph({}) }} />
);
