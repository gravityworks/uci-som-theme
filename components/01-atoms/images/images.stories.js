import React from 'react';

import image from './image/responsive-image.twig';
import figure from './image/figure.twig';
import iconTwig from './icons/icons.twig';
// import infographicTwig from './infographics/infographics.twig';
import logoTwig from './logo/logo.twig';
import imageLinkTemplate from './imageLink.twig';
import infographicImageTemplate from './infographicImage/infographicImage.twig';

import imageData from './image/image.yml';
import figureData from './image/figure.yml';
import iconData from './icons/icons.yml';
// import infographicData from './infographics/infographics.yml';
import logoData from './logo/logo.yml';
import iconImageData from './iconImage/iconImage.yml';
import iconImageAltData from './iconImage/iconImage--alt.yml';
import imageLinkData from './imageLink/imageLink.yml';
import infographicImageData from './infographicImage/infographicImage.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Atoms/Images' };

export const images = () => (
  <div dangerouslySetInnerHTML={{ __html: image(imageData) }} />
);
export const figures = () => (
  <div dangerouslySetInnerHTML={{ __html: figure(figureData) }} />
);
export const themeIcons = () => (
  <div dangerouslySetInnerHTML={{ __html: iconTwig(iconData) }} />
);
// export const infographics = () => (
//   <div dangerouslySetInnerHTML={{ __html: infographicTwig(infographicData) }} />
// );
export const logo = () => (
  <div dangerouslySetInnerHTML={{ __html: logoTwig(logoData) }} />
);

export const iconImage = () => (
  <div
    className="sb-iconImage"
    dangerouslySetInnerHTML={{ __html: imageLinkTemplate(iconImageData) }}
  />
);

export const iconImageAlt = () => (
  <div
    className="sb-iconImage sb-iconImage--alt"
    dangerouslySetInnerHTML={{ __html: imageLinkTemplate(iconImageAltData) }}
  />
);

export const imageLink = () => (
  <div dangerouslySetInnerHTML={{ __html: imageLinkTemplate(imageLinkData) }} />
);

export const infographicImage = () => (
  <div
    className="sb-infogaphicImage"
    dangerouslySetInnerHTML={{
      __html: infographicImageTemplate(infographicImageData),
    }}
  />
);
