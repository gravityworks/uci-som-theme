// Storybook components
import React from 'react';
import Button from './react/Button.component';

// Twig Libraries
import button from './twig/button.twig';
import siteTab from './twig/siteTab.twig';
import coloredButton from './twig/coloredButton.twig';
import transparentButton from './twig/transparentButton.twig';
import iconButton from './twig/iconButton.twig';
import shareButton from './twig/shareButton.twig';

// YML Data
import primaryButtonData from './yml/button-primary.yml';
import primaryButtonExternalLinkData from './yml/button-primary--external-link.yml';
import primaryButtonActiveData from './yml/button-primary--active.yml';
import primaryButtonFullWidthData from './yml/button-primary--full-width.yml';
import extraLargeButtonData from './yml/button-extra-large.yml';
import ghostButtonData from './yml/button-ghost.yml';
import ghostButtonExternalLinkData from './yml/button-ghost--external-link.yml';
import iconButtonData from './yml/button-icon.yml';
import siteTabData from './yml/siteTab.yml';
import siteTabActiveData from './yml/siteTab--active.yml';
import tag from './yml/button-tag.yml';

// Colored Buttons
import coloredButtonBlueData from './yml/colored-buttons/colored-button-blue.yml';
import coloredButtonGreenData from './yml/colored-buttons/colored-button-green.yml';
import coloredButtonYellowData from './yml/colored-buttons/colored-button-yellow.yml';
// Transparent Buttons
import transparentBlueData from './yml/transparent-buttons/transparent-blue.yml';
import transparentOrangeData from './yml/transparent-buttons/transparent-orange.yml';
import transparentGreenData from './yml/transparent-buttons/transparent-green.yml';
import transparentYellowData from './yml/transparent-buttons/transparent-yellow.yml';
import transparentGrayData from './yml/transparent-buttons/transparent-gray.yml';
// Icon Buttons
import menuIconButton from './yml/icon-button/button-icon-menu.yml';
import menuIconTextButton from './yml/icon-button/button-icon-menu-text.yml';
import arrowIconButton from './yml/icon-button/button-icon-arrow.yml';
import searchIconButton from './yml/icon-button/button-icon-search.yml';
import searchIconAltButton from './yml/icon-button/button-icon-search-alt.yml';
// Share Buttons
import facebookButton from './yml/share-buttons/button-share-facebook.yml';
import facebookButtonAlt from './yml/share-buttons/button-share-facebook-alt.yml';
import twitterButton from './yml/share-buttons/button-share-twitter.yml';
import twitterButtonAlt from './yml/share-buttons/button-share-twitter-alt.yml';
import instagramButton from './yml/share-buttons/button-share-instagram.yml';
import instagramButtonAlt from './yml/share-buttons/button-share-instagram-alt.yml';
import printerButton from './yml/share-buttons/button-share-printer.yml';
import printerButtonAlt from './yml/share-buttons/button-share-printer-alt.yml';
import emailButton from './yml/share-buttons/button-share-email.yml';
import emailButtonAlt from './yml/share-buttons/button-share-email-alt.yml';

/**
 * Storybook Definition.
 */
export default {
  component: Button,
  title: 'Atoms/Button',
};

export const primary = () => (
  <>
    <div dangerouslySetInnerHTML={{ __html: button(primaryButtonData) }} />
    <br />
    <div
      dangerouslySetInnerHTML={{
        __html: button(primaryButtonExternalLinkData),
      }}
    />
    <br />
    <div
      dangerouslySetInnerHTML={{ __html: button(primaryButtonActiveData) }}
    />
    <br />
    <div
      dangerouslySetInnerHTML={{ __html: button(primaryButtonFullWidthData) }}
    />
  </>
);

export const ghost = () => (
  <div className="sg-ghost-button-wrapper">
    <div dangerouslySetInnerHTML={{ __html: button(ghostButtonData) }} />
    <br />
    <div
      dangerouslySetInnerHTML={{ __html: button(ghostButtonExternalLinkData) }}
    />
  </div>
);

export const extraLarge = () => (
  <div>
    <div dangerouslySetInnerHTML={{ __html: button(extraLargeButtonData) }} />
  </div>
);

export const buttonWithIcon = () => (
  <div dangerouslySetInnerHTML={{ __html: button(iconButtonData) }} />
);

export const siteTabButton = () => (
  <>
    <div dangerouslySetInnerHTML={{ __html: siteTab(siteTabData) }} />
    <br />
    <div dangerouslySetInnerHTML={{ __html: siteTab(siteTabActiveData) }} />
  </>
);

export const coloredButtons = () => (
  <>
    <div
      dangerouslySetInnerHTML={{
        __html: coloredButton(coloredButtonBlueData),
      }}
    />
    <br />
    <div
      dangerouslySetInnerHTML={{
        __html: coloredButton(coloredButtonGreenData),
      }}
    />
    <br />
    <div
      dangerouslySetInnerHTML={{
        __html: coloredButton(coloredButtonYellowData),
      }}
    />
  </>
);

export const transparentButtons = () => (
  <>
    <div
      dangerouslySetInnerHTML={{
        __html: transparentButton(transparentBlueData),
      }}
    />
    <br />
    <div
      dangerouslySetInnerHTML={{
        __html: transparentButton(transparentOrangeData),
      }}
    />
    <br />
    <div
      dangerouslySetInnerHTML={{
        __html: transparentButton(transparentGreenData),
      }}
    />
    <br />
    <div
      dangerouslySetInnerHTML={{
        __html: transparentButton(transparentYellowData),
      }}
    />
    <br />
    <div
      dangerouslySetInnerHTML={{
        __html: transparentButton(transparentGrayData),
      }}
    />
    <br />
  </>
);

export const iconButtons = () => (
  <>
    <div
      dangerouslySetInnerHTML={{
        __html: iconButton(menuIconButton),
      }}
    />
    <br />
    <div
      dangerouslySetInnerHTML={{
        __html: iconButton(menuIconTextButton),
      }}
    />
    <br />
    <div
      dangerouslySetInnerHTML={{
        __html: iconButton(arrowIconButton),
      }}
    />
    <br />
    <div
      dangerouslySetInnerHTML={{
        __html: iconButton(searchIconButton),
      }}
    />
    <br />
    <div
      className="sg-icon-search-alt"
      dangerouslySetInnerHTML={{
        __html: iconButton(searchIconAltButton),
      }}
    />
  </>
);

export const tags = () => (
  <div
    dangerouslySetInnerHTML={{
      __html: button(tag),
    }}
  />
);

export const shareButtons = () => (
  <div className="sg-share-buttons">
    <div className="sg-share-buttons-blue">
      <div dangerouslySetInnerHTML={{ __html: shareButton(facebookButton) }} />
      <div dangerouslySetInnerHTML={{ __html: shareButton(twitterButton) }} />
      <div dangerouslySetInnerHTML={{ __html: shareButton(instagramButton) }} />
      <div dangerouslySetInnerHTML={{ __html: shareButton(printerButton) }} />
      <div dangerouslySetInnerHTML={{ __html: shareButton(emailButton) }} />
    </div>
    <br />
    <div className="sg-share-buttons-alt">
      <div
        dangerouslySetInnerHTML={{ __html: shareButton(facebookButtonAlt) }}
      />
      <div
        dangerouslySetInnerHTML={{ __html: shareButton(twitterButtonAlt) }}
      />
      <div
        dangerouslySetInnerHTML={{ __html: shareButton(instagramButtonAlt) }}
      />
      <div
        dangerouslySetInnerHTML={{ __html: shareButton(printerButtonAlt) }}
      />
      <div dangerouslySetInnerHTML={{ __html: shareButton(emailButtonAlt) }} />
    </div>
  </div>
);
