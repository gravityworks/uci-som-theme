import React from 'react';

import regionTemplate from './region.twig';

import regionData from './region.yml';
import regionBlueData from './region--blue.yml';
import regionGrayData from './region--gray.yml';

export default { title: 'Atoms/Regions' };

export const defaultRegion = () => (
  <div
    className="sg-regions"
    dangerouslySetInnerHTML={{ __html: regionTemplate(regionData) }}
  />
);

export const blueRegion = () => (
  <div
    className="sg-regions"
    dangerouslySetInnerHTML={{ __html: regionTemplate(regionBlueData) }}
  />
);

export const grayRegion = () => (
  <div
    className="sg-regions"
    dangerouslySetInnerHTML={{ __html: regionTemplate(regionGrayData) }}
  />
);
