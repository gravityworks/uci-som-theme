import React from 'react';

import horiontalRule from './horizontalRule.twig';

import hrYellowData from './horizontalRule_yellow.yml';
import hrYellowNarrowData from './horizontalRule_yellow_narrow.yml';
import hrThickData from './horizontalRule_thick.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Atoms/Horizontal Rules' };

export const horizontalRule = () => (
  <div dangerouslySetInnerHTML={{ __html: horiontalRule() }} />
);

export const horizontalRuleThick = () => (
  <div dangerouslySetInnerHTML={{ __html: horiontalRule(hrThickData) }} />
);

export const horizontalRuleYellow = () => (
  <div dangerouslySetInnerHTML={{ __html: horiontalRule(hrYellowData) }} />
);

export const horizontalRuleYellowNarrow = () => (
  <div
    dangerouslySetInnerHTML={{ __html: horiontalRule(hrYellowNarrowData) }}
  />
);
