import React from 'react';

import tableSectionTemplate from './table_section.twig';
import tableTemplate from './table.twig';

import tableSectionData from './table_rows.yml';
import tableData from './table.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Atoms/Tables' };

export const tableSection = () => (
  <table
    dangerouslySetInnerHTML={{ __html: tableSectionTemplate(tableSectionData) }}
  />
);

export const table = () => (
  <div dangerouslySetInnerHTML={{ __html: tableTemplate(tableData) }} />
);
