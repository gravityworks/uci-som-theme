import React from 'react';

import link from './link.twig';
import linkSocial from './link-social.twig';
import linkListTemplate from './link-list.twig';

import linkData from './yml/link.yml';
import linkArrowData from './yml/link-arrow.yml';
import linkIconData from './yml/link-icon.yml';
import linkIconBeforeData from './yml/link-icon--before.yml';
import linkIconAfterData from './yml/link-icon--after.yml';
import linkSocialData from './yml/link-social.yml';
import linkSocialWhiteData from './yml/link-social--white.yml';
import linkSocialBlueData from './yml/link-social--blue.yml';
import linkListData from './yml/link-list.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Atoms/Links' };

export const links = () => (
  <div dangerouslySetInnerHTML={{ __html: link(linkData) }} />
);

export const linkArrow = () => (
  <div dangerouslySetInnerHTML={{ __html: link(linkArrowData) }} />
);

export const linkIcon = () => (
  <div dangerouslySetInnerHTML={{ __html: link(linkIconData) }} />
);

export const linkIconBefore = () => (
  <div dangerouslySetInnerHTML={{ __html: link(linkIconBeforeData) }} />
);

export const linkIconAfter = () => (
  <div dangerouslySetInnerHTML={{ __html: link(linkIconAfterData) }} />
);

export const linkSocialIcon = () => (
  <div
    className="sg-linkSocialIcon"
    dangerouslySetInnerHTML={{ __html: linkSocial(linkSocialData) }}
  />
);

export const linkSocialIconWhite = () => (
  <div
    className="sg-linkSocialIcon"
    dangerouslySetInnerHTML={{ __html: linkSocial(linkSocialWhiteData) }}
  />
);
export const linkSocialIconBlue = () => (
  <div
    className="sg-linkSocialIcon blue"
    dangerouslySetInnerHTML={{ __html: linkSocial(linkSocialBlueData) }}
  />
);

export const linkList = () => (
  <div dangerouslySetInnerHTML={{ __html: linkListTemplate(linkListData) }} />
);
