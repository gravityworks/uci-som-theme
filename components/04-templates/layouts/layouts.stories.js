import React from 'react';

import placeHolderTwig from '../placeholder/place-holder.twig';

/**
 * Storybook Definition.
 */
export default { title: 'Templates/Layouts' };

export const newsDetail = () => (
  <div className="container--news">
    <div
      className="content"
      dangerouslySetInnerHTML={{
        __html: placeHolderTwig({ place_holder: 'Main Content' }),
      }}
    />
    <div
      className="sidebar"
      dangerouslySetInnerHTML={{
        __html: placeHolderTwig({ place_holder: 'Sidebar' }),
      }}
    />
  </div>
);

export const fullBleed = () => (
  <div className="container--full-bleed">
    <div
      dangerouslySetInnerHTML={{
        __html: placeHolderTwig({ place_holder: 'Full Bleed' }),
      }}
    />
  </div>
);

export const fullWidth = () => (
  <div className="container">
    <div
      dangerouslySetInnerHTML={{
        __html: placeHolderTwig({ place_holder: 'Full Width' }),
      }}
    />
  </div>
);

export const fiftyFifty = () => (
  <div className="container--fifty-fifty">
    <div
      dangerouslySetInnerHTML={{
        __html: placeHolderTwig({ place_holder: 'Fifty / Fifty' }),
      }}
    />
    <div
      dangerouslySetInnerHTML={{
        __html: placeHolderTwig({ place_holder: 'Fifty / Fifty' }),
      }}
    />
  </div>
);

export const sideBarRight = () => (
  <div className="container--sidebar-right">
    <div
      dangerouslySetInnerHTML={{
        __html: placeHolderTwig({ place_holder: 'Main Content' }),
      }}
    />
    <div
      dangerouslySetInnerHTML={{
        __html: placeHolderTwig({ place_holder: 'Sidebar' }),
      }}
    />
  </div>
);

export const sideBarLeft = () => (
  <div className="container--sidebar-left">
    <div
      dangerouslySetInnerHTML={{
        __html: placeHolderTwig({ place_holder: 'Sidebar' }),
      }}
    />
    <div
      dangerouslySetInnerHTML={{
        __html: placeHolderTwig({ place_holder: 'Main Content' }),
      }}
    />
  </div>
);

export const centerContent = () => (
  <div className="container--center-content">
    <div
      dangerouslySetInnerHTML={{
        __html: placeHolderTwig({ place_holder: 'Center Content' }),
      }}
    />
  </div>
);
