import React from 'react';

import placeHolderTwig from './place-holder.twig';

/**
 * Storybook Definition.
 */
export default { title: 'Templates/Place Holder' };

export const placeHolder = () => (
  <div
    className="sg-form--blue"
    dangerouslySetInnerHTML={{
      __html: placeHolderTwig({ place_holder: 'Place Holder' }),
    }}
  />
);
