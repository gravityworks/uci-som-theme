import React from 'react';

import personasTemplate from './personas.twig';

import personaData from './persona_items.yml';

import './personas.js';

export default { title: 'Paragraphs/Personas' };

export const panels = () => (
  <div dangerouslySetInnerHTML={{ __html: personasTemplate(personaData) }} />
);
