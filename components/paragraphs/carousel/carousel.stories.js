import React from 'react';
// import { useEffect } from '@storybook/client-api';

import carouselTemplate from './carousel.twig';
import carouselData from './carousel.yml';

// JS
import './carousel';

export default { title: 'Paragraphs/Carousel' };

export const carousel = () => {
  // useEffect(() => Drupal.attachBehaviors(), []);
  return (
    <div dangerouslySetInnerHTML={{ __html: carouselTemplate(carouselData) }} />
  );
};
