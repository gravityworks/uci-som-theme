import React from 'react';

// Twig Templates
import fiftyFiftyTemplate from './fiftyFifty/callout-fiftyFifty.twig';
import calloutCommon from './callout-common.twig';
import basicTemplate from './basic/basicCallout.twig';
import calloutMap from './map/callout-map.twig';
import miniMenuTemplate from './miniMenu/miniMenu.twig';

// YML Files
import basicData from './basic/basicCallout.yml';
import basicDataButton from './basic/basicCallout_button.yml';
import basicDataGray from './basic/basicCallout_gray.yml';
import infographicData from './icon/iconCallout-infographic.yml';
import buttonData from './icon/iconCallout-buttons.yml';
import buttonBlueData from './icon/iconCallout-buttons--blue.yml';
import fiftyFiftyData from './fiftyFifty/callout-fiftyFifty.yml';
import calloutMapData from './map/callout-map.yml';
import calloutMapGrayData from './map/callout-map-gray.yml';
import calloutTopicsData from './topics/topics.yml';
import miniMenuData from './miniMenu/miniMenu.yml';
import miniMenuNoBarTitleData from './miniMenu/miniMenu_noBarTitle.yml';

// Gradient Callouts (Common TWIG)
import gradientData from './gradient/yml/callout-gradient.yml';
import gradientReverseData from './gradient/yml/callout-gradient--reverse.yml';
import gradientPhotoData from './gradient/yml/callout-gradient-photo.yml';
import gradientPhotoReverseData from './gradient/yml/callout-gradient-photo--reverse.yml';
import gradientButtonGhostData from './gradient/yml/callout-gradient--button-ghost.yml';

// Photo Callouts (Commong TWIG)
import photoData from './photo/callout-photo.yml';
import photoDataGray from './photo/callout-photo--gray.yml';
import photoDataWhite from './photo/callout-photo--white.yml';
import photoReverseData from './photo/callout-photo--reverse.yml';

// Featured Callouts (Common TWIG)
import featuredData from './featured/callout-featured.yml';
import featuredReverseData from './featured/callout-featured--reverse.yml';
// Featured Callouts

/**
 * Storybook Definition.
 */
export default { title: 'Paragraphs/Callouts' };

export const basicCallout = () => (
  <>
    <div dangerouslySetInnerHTML={{ __html: basicTemplate(basicData) }} />
    <br />
    <div dangerouslySetInnerHTML={{ __html: basicTemplate(basicDataButton) }} />
    <br />
    <div dangerouslySetInnerHTML={{ __html: basicTemplate(basicDataGray) }} />
    <br />
    <div dangerouslySetInnerHTML={{ __html: basicTemplate(buttonData) }} />
    <br />
    <div dangerouslySetInnerHTML={{ __html: basicTemplate(buttonBlueData) }} />
  </>
);

export const infographics = () => (
  <div dangerouslySetInnerHTML={{ __html: basicTemplate(infographicData) }} />
);

export const fiftyFifty = () => (
  <div
    dangerouslySetInnerHTML={{ __html: fiftyFiftyTemplate(fiftyFiftyData) }}
  />
);

export const gradient = () => (
  <>
    <div dangerouslySetInnerHTML={{ __html: calloutCommon(gradientData) }} />
    <br />
    <div
      dangerouslySetInnerHTML={{ __html: calloutCommon(gradientReverseData) }}
    />
    <br />
    <div
      dangerouslySetInnerHTML={{ __html: calloutCommon(gradientPhotoData) }}
    />
    <br />
    <div
      dangerouslySetInnerHTML={{
        __html: calloutCommon(gradientPhotoReverseData),
      }}
    />
    <br />
    <div
      dangerouslySetInnerHTML={{
        __html: calloutCommon(gradientButtonGhostData),
      }}
    />
  </>
);

export const photo = () => (
  <>
    <div dangerouslySetInnerHTML={{ __html: calloutCommon(photoData) }} />
    <br />
    <div
      dangerouslySetInnerHTML={{ __html: calloutCommon(photoReverseData) }}
    />
    <br />
    <div dangerouslySetInnerHTML={{ __html: calloutCommon(photoDataGray) }} />
    <br />
    <div dangerouslySetInnerHTML={{ __html: calloutCommon(photoDataWhite) }} />
  </>
);

export const featured = () => (
  <>
    <div dangerouslySetInnerHTML={{ __html: calloutCommon(featuredData) }} />
    <br />
    <div
      dangerouslySetInnerHTML={{ __html: calloutCommon(featuredReverseData) }}
    />
  </>
);

export const map = () => (
  <div dangerouslySetInnerHTML={{ __html: calloutMap(calloutMapGrayData) }} />
);

export const topic = () => (
  <div dangerouslySetInnerHTML={{ __html: basicTemplate(calloutTopicsData) }} />
);

export const miniMenu = () => (
  <>
    <div dangerouslySetInnerHTML={{ __html: miniMenuTemplate(miniMenuData) }} />
    <br />
    <div
      dangerouslySetInnerHTML={{
        __html: miniMenuTemplate(miniMenuNoBarTitleData),
      }}
    />
  </>
);
