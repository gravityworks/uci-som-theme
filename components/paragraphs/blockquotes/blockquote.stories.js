import React from 'react';

import blockquotes from '../../02-molecules/blockquotes/blockquote.twig';

import blockquoteData from '../../02-molecules/blockquotes/yml/blockquote.yml';
import blockquoteImageData from '../../02-molecules/blockquotes/yml/blockquote-image.yml';
import blockquoteImageRightData from '../../02-molecules/blockquotes/yml/blockquote-image-right.yml';
import blockquoteDarkData from '../../02-molecules/blockquotes/yml/blockquote-dark.yml';
// import blockquoteDarkImageData from '../../02-molecules/blockquote/yml/blockquote-dark-image.yml';
import blockquoteYellowData from '../../02-molecules/blockquotes/yml/blockquote-yellow.yml';
import blockquoteYellowImageData from '../../02-molecules/blockquotes/yml/blockquote-yellow-image.yml';

/**
 *  Storybook Definition.
 */
export default { title: 'Paragraphs/Blockquotes' };

export const blockquote = () => (
  <div dangerouslySetInnerHTML={{ __html: blockquotes(blockquoteData) }} />
);

export const blockquoteWithImage = () => (
  <div dangerouslySetInnerHTML={{ __html: blockquotes(blockquoteImageData) }} />
);

export const blockquoteWithImageRight = () => (
  <div
    dangerouslySetInnerHTML={{ __html: blockquotes(blockquoteImageRightData) }}
  />
);

export const blockquoteDark = () => (
  <div dangerouslySetInnerHTML={{ __html: blockquotes(blockquoteDarkData) }} />
);

export const blockquoteDarkWithImage = () => (
  <div
    dangerouslySetInnerHTML={{ __html: blockquotes(blockquoteDarkImageData) }}
  />
);

export const blockquoteYellow = () => (
  <div
    dangerouslySetInnerHTML={{ __html: blockquotes(blockquoteYellowData) }}
  />
);

export const blockquoteYellowWithImage = () => (
  <div
    dangerouslySetInnerHTML={{ __html: blockquotes(blockquoteYellowImageData) }}
  />
);
