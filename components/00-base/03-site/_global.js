Drupal.behaviors.global = {
  attach(context) {
    // Remove the duplicate buttons on News/Events Views
    console.log('Global.js script loaded')
    const viewAllLink = document.querySelector('.view .view-all-cta > .link');
    if (viewAllLink) {
      const viewAllButton = document.querySelector('.view .view-content + a.button');
      if (viewAllButton) {
        viewAllButton.style.display = 'none';
      }
    }
  },
};
