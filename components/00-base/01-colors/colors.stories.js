import React from 'react';

import colors from './colors.twig';

import colorsPrimary from './colors-primary.yml';
import colorsSecondary from './colors-secondary.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Base/Colors' };

export const Primary = () => (
  <div dangerouslySetInnerHTML={{ __html: colors(colorsPrimary) }} />
);

export const Secondary = () => (
  <div dangerouslySetInnerHTML={{ __html: colors(colorsSecondary) }} />
);
