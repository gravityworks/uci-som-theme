const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const globImporter = require('node-sass-glob-importer');
// const { icons } = require('../components/01-atoms/images/images.stories');

const ImageLoader = {
  test: /\.(png|svg|jpg|gif)$/i,
  exclude: /icons\/.*\.svg$/,
  loader: 'file-loader',
};

const CSSLoader = {
  test: /\.s[ac]ss$/i,
  exclude: /node_modules/,
  use: [
    MiniCssExtractPlugin.loader,
    {
      loader: 'css-loader',
    },
    {
      loader: 'postcss-loader',
      options: {
        config: {
          path: path.resolve('./webpack/'),
        },
      },
    },
    {
      loader: 'sass-loader',
      options: {
        sassOptions: {
          importer: globImporter(),
          outputStyle: 'compressed',
        },
      },
    },
  ],
};

const SVGSpriteLoader = {
  test: /images\/.*\.svg$/, // your icons directory
  loader: 'svg-sprite-loader',
  options: {
    extract: true,
    spriteFilename: '../dist/icons.svg',
  },
};

// const SVGSpriteLoader = {
//   test: /\.svg$/,
//   include: [
//     path.resolve(__dirname, '/images/icons'),
//     path.resolve(__dirname, '/images/infographics'),
//   ],
//   loader: 'svg-sprite-loader',
//   options: {
//     // spriteFilename: '../dist/icons.svg',
//     spriteFilename: (svgPath) =>
//       svgPath.includes('icons')
//         ? '../dist/icons.svg'
//         : '../dist/infographics.svg',
//     extract: true,
//   },
// };

module.exports = {
  CSSLoader,
  SVGSpriteLoader,
  ImageLoader,
};
