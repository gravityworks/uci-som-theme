const { RemoteBrowserTarget } = require('happo.io');

const baseURL = 'https://medschool.uci.edu';

module.exports = {
  apiKey: 'bd58d937ac',
  apiSecret: '973afa8d3ac3a562096e596dc',
  project: 'UCI School of Medicine - Main Site',
  pages: [
    {
      url: `${baseURL}`,
      title: 'Home',
    },
    {
      url: `${baseURL}/research`,
      title: 'Research',
    },
    {
      url: `${baseURL}/education`,
      title: 'Education',
    },
    {
      url: `${baseURL}/healthcare`,
      title: 'Healthcare',
    },
    {
      url: `${baseURL}/community`,
      title: 'Community',
    },
    {
      url: `${baseURL}/about`,
      title: 'About',
    },
    {
      url: `${baseURL}/news`,
      title: 'News',
    },
    {
      url: `${baseURL}/events`,
      title: 'Events',
    },
    {
      url: `${baseURL}/community/giving`,
      title: 'Giving',
    },
    {
      url: `${baseURL}/about/contact-us`,
      title: 'Contact Us',
    },
    {
      url: `${baseURL}/research/office-research-leadership-staff`,
      title: 'Office of Research Leadership Staff',
    },
    {
      url: `${baseURL}/research/research-clinical-departments`,
      title: 'Research Clinical Departments',
    },
    {
      url: `${baseURL}/research/centers-institutes`,
      title: 'Centers & Institutes',
    },
    {
      url: `${baseURL}/education/medical-education`,
      title: 'Medical Education',
    },
    {
      url: `${baseURL}/about/why-uci-school-medicine`,
      title: 'Why UCI School of Medicine',
    },
    {
      url: `${baseURL}/education/admissions`,
      title: 'Admissions',
    },
    {
      url: `${baseURL}/about/dean-michael-j-stamos-md`,
      title: 'Dean Stamos',
    },
    {
      url: `${baseURL}/community/willed-body-program/donor-registration-packets`,
      title: 'Willed Body Program Donor Registration Packets',
    },
  ],
  targets: {
    'chrome-desktop': new RemoteBrowserTarget('chrome', {
      viewport: '1366x768',
    }),
    'firefox-desktop': new RemoteBrowserTarget('firefox', {
      viewport: '1366x768',
    }),
    'safari-desktop': new RemoteBrowserTarget('safari', {
      viewport: '1366x768',
      scrollStitch: true,
    }),
    'edge-desktop': new RemoteBrowserTarget('edge', {
      viewport: '1366x768',
    }),
    'chrome-tablet': new RemoteBrowserTarget('chrome', {
      viewport: '768x1024',
    }),
    'firefox-tablet': new RemoteBrowserTarget('firefox', {
      viewport: '768x1024',
    }),
    'safari-tablet': new RemoteBrowserTarget('safari', {
      viewport: '768x1024',
      scrollStitch: true,
    }),
    'edge-tablet': new RemoteBrowserTarget('edge', {
      viewport: '768x1024',
    }),
    'chrome-mobile': new RemoteBrowserTarget('chrome', {
      viewport: '360x640',
    }),
    'firefox-mobile': new RemoteBrowserTarget('firefox', {
      viewport: '360x640',
    }),
    'safari-mobile': new RemoteBrowserTarget('safari', {
      viewport: '360x640',
      scrollStitch: true,
    }),
    'edge-mobile': new RemoteBrowserTarget('edge', {
      viewport: '400x711',
    }),
    'ios-safari': new RemoteBrowserTarget('ios-safari', {
      viewport: '375x667',
    }),
    'chrome-desktop-xl': new RemoteBrowserTarget('chrome', {
      viewport: '1920x1080',
    }),
    'firefox-desktop-xl': new RemoteBrowserTarget('firefox', {
      viewport: '1920x1080',
    }),
    'safari-desktop-xl': new RemoteBrowserTarget('safari', {
      viewport: '1920x1080',
      scrollStitch: true,
    }),
    'edge-desktop-xl': new RemoteBrowserTarget('edge', {
      viewport: '1920x1080',
    }),
  },
};
